<svg id="anim-s vg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 550.48 552.78">
    <defs>
        <style>
        .cls-1 {
            mask: url(#mask);
        }

        .cls-2, .cls-26, .cls-27, .cls-28, .cls-30, .cls-4 {
            mix-blend-mode: multiply;
        }

        .cls-2 {
            fill: url(#radial-gradient);
        }

        .cls-3 {
            mask: url(#mask-2);
        }

        .cls-4 {
            fill: url(#radial-gradient-2);
        }

        .cls-5 {
            isolation: isolate;
        }

        .cls-6 {
            fill: #5c4a4d;
        }

        .cls-7 {
            fill: #77676a;
        }

        .cls-8 {
            fill: #988889;
        }

        .cls-74, .cls-75, .cls-9 {
            fill: none;
            stroke-miterlimit: 10;
        }

        .cls-9 {
            stroke: #00ffe2;
            stroke-width: 0.74px;
        }

        .cls-10 {
            fill: #f6877a;
        }

        .cls-11 {
            fill: #b2a6a7;
        }

        .cls-12 {
            fill: #c7b8bb;
        }

        .cls-13 {
            fill: #f7978b;
        }

        .cls-14 {
            fill: #ffc3a5;
        }

        .cls-15 {
            fill: #503d40;
        }

        .cls-16 {
            fill: #3b2b3b;
        }

        .cls-17 {
            fill: #87797c;
        }

        .cls-18 {
            fill: #d4c8cb;
        }

        .cls-19 {
            fill: #ded5d7;
        }

        .cls-20 {
            fill: #453645;
        }

        .cls-21 {
            fill: #c08750;
        }

        .cls-22 {
            fill: #d79c58;
        }

        .cls-23 {
            fill: url(#linear-gradient);
        }

        .cls-24, .cls-47, .cls-49, .cls-51, .cls-53 {
            opacity: 0.5;
        }

        .cls-24, .cls-29, .cls-31, .cls-32, .cls-33, .cls-34, .cls-35, .cls-36, .cls-37, .cls-38, .cls-39, .cls-40, .cls-41, .cls-43, .cls-45, .cls-47, .cls-49, .cls-51, .cls-53, .cls-54, .cls-55, .cls-56, .cls-57, .cls-58, .cls-59, .cls-60, .cls-61, .cls-62, .cls-63, .cls-64, .cls-65, .cls-66, .cls-67, .cls-68 {
            mix-blend-mode: screen;
        }

        .cls-24 {
            fill: url(#linear-gradient-2);
        }

        .cls-25, .cls-26, .cls-27, .cls-29, .cls-31, .cls-32 {
            fill: #ca9d5e;
        }

        .cls-27, .cls-31 {
            opacity: 0.6;
        }

        .cls-28 {
            fill: #957c40;
        }

        .cls-29 {
            opacity: 0.3;
        }

        .cls-30 {
            fill: #7c5a31;
        }

        .cls-32 {
            opacity: 0.7;
        }

        .cls-33 {
            opacity: 0.4;
            fill: url(#linear-gradient-3);
        }

        .cls-34 {
            fill: url(#linear-gradient-4);
        }

        .cls-35 {
            fill: url(#linear-gradient-5);
        }

        .cls-36 {
            fill: url(#linear-gradient-6);
        }

        .cls-37 {
            fill: url(#linear-gradient-7);
        }

        .cls-38 {
            fill: url(#linear-gradient-8);
        }

        .cls-39 {
            fill: url(#linear-gradient-9);
        }

        .cls-40 {
            fill: url(#linear-gradient-10);
        }

        .cls-41 {
            fill: url(#linear-gradient-11);
        }

        .cls-42 {
            mask: url(#mask-3);
        }

        .cls-43 {
            fill: url(#radial-gradient-3);
        }

        .cls-44 {
            mask: url(#mask-4);
        }

        .cls-45 {
            fill: url(#radial-gradient-4);
        }

        .cls-46 {
            fill: url(#linear-gradient-12);
        }

        .cls-47 {
            fill: url(#linear-gradient-13);
        }

        .cls-48 {
            fill: url(#linear-gradient-14);
        }

        .cls-49 {
            fill: url(#linear-gradient-15);
        }

        .cls-50 {
            fill: url(#linear-gradient-16);
        }

        .cls-51 {
            fill: url(#linear-gradient-17);
        }

        .cls-52 {
            fill: url(#linear-gradient-18);
        }

        .cls-53 {
            fill: url(#linear-gradient-19);
        }

        .cls-54 {
            fill: url(#linear-gradient-20);
        }

        .cls-55 {
            fill: url(#linear-gradient-21);
        }

        .cls-56 {
            fill: url(#linear-gradient-22);
        }

        .cls-57 {
            fill: url(#linear-gradient-23);
        }

        .cls-58 {
            fill: url(#linear-gradient-24);
        }

        .cls-59 {
            fill: url(#linear-gradient-25);
        }

        .cls-60 {
            fill: url(#linear-gradient-26);
        }

        .cls-61 {
            fill: url(#linear-gradient-27);
        }

        .cls-62 {
            fill: url(#linear-gradient-28);
        }

        .cls-63 {
            fill: url(#linear-gradient-29);
        }

        .cls-64 {
            fill: url(#linear-gradient-30);
        }

        .cls-65 {
            fill: url(#linear-gradient-31);
        }

        .cls-66 {
            fill: url(#linear-gradient-32);
        }

        .cls-67 {
            fill: url(#linear-gradient-33);
        }

        .cls-68 {
            fill: url(#linear-gradient-34);
        }

        .cls-69 {
            fill: url(#linear-gradient-35);
        }

        .cls-70 {
            fill: url(#linear-gradient-36);
        }

        .cls-71 {
            fill: url(#linear-gradient-37);
        }

        .cls-72 {
            fill: url(#linear-gradient-38);
        }

        .cls-73 {
            fill: url(#linear-gradient-39);
        }

        .cls-74 {
            stroke: #662d91;
        }

        .cls-75 {
            stroke: red;
        }

        .cls-76 {
            fill: #231f22;
        }

        .cls-77 {
            fill: url(#radial-gradient-5);
        }

        .cls-78 {
            fill: #ff771f;
        }

        .cls-79 {
            fill: #f6e6d4;
        }

        .cls-80 {
            fill: #ff6800;
        }

        .cls-81 {
            fill: #f7331b;
        }

        .cls-82 {
            fill: #fff;
        }

        .cls-83 {
            fill: #ffcd00;
        }

        .cls-84 {
            fill: url(#Dégradé_sans_nom_93);
        }

        .cls-85 {
            fill: url(#Dégradé_sans_nom_93-2);
        }

        .cls-86 {
            fill: url(#Dégradé_sans_nom_93-3);
        }

        .cls-87 {
            fill: url(#Dégradé_sans_nom_93-4);
        }

        .cls-88 {
            fill: url(#Dégradé_sans_nom_93-5);
        }

        .cls-89 {
            fill: url(#Dégradé_sans_nom_93-6);
        }

        .cls-90 {
            fill: url(#Dégradé_sans_nom_93-7);
        }

        .cls-91 {
            fill: url(#Dégradé_sans_nom_93-8);
        }

        .cls-92 {
            fill: url(#Dégradé_sans_nom_93-9);
        }

        .cls-93 {
            fill: url(#Dégradé_sans_nom_93-10);
        }

        .cls-94 {
            fill: url(#Dégradé_sans_nom_93-11);
        }

        .cls-95 {
            fill: url(#Dégradé_sans_nom_93-12);
        }

        .cls-96 {
            fill: url(#Dégradé_sans_nom_93-13);
        }

        .cls-97 {
            fill: url(#Dégradé_sans_nom_93-14);
        }

        .cls-98 {
            fill: url(#Dégradé_sans_nom_93-15);
        }

        .cls-99 {
            fill: url(#Dégradé_sans_nom_93-16);
        }

        .cls-100 {
            fill: url(#Dégradé_sans_nom_93-17);
        }

        .cls-101 {
            fill: url(#Dégradé_sans_nom_93-18);
        }

        .cls-102 {
            fill: url(#Dégradé_sans_nom_93-19);
        }

        .cls-103 {
            fill: url(#Dégradé_sans_nom_93-20);
        }

        .cls-104 {
            fill: url(#Dégradé_sans_nom_93-21);
        }

        .cls-105 {
            fill: url(#Dégradé_sans_nom_93-22);
        }

        .cls-106 {
            fill: url(#Dégradé_sans_nom_93-23);
        }

        .cls-107 {
            fill: url(#Dégradé_sans_nom_93-24);
        }

        .cls-108 {
            filter: url(#luminosity-noclip-4);
        }

        .cls-109 {
            filter: url(#luminosity-noclip-3);
        }

        .cls-110 {
            filter: url(#luminosity-noclip-2);
        }

        .cls-111 {
            filter: url(#luminosity-noclip);
        }
        </style>
        <filter id="luminosity-noclip" x="-35.27" y="-8442" width="62.59" height="32766" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-color="#fff" result="bg"/>
        <feBlend in="SourceGraphic" in2="bg"/>
        </filter>
        <mask id="mask" x="-35.27" y="-8442" width="62.59" height="32766" maskUnits="userSpaceOnUse">
        <g class="cls-111"/>
        </mask>
        <radialGradient id="radial-gradient" cx="222.49" cy="24.8" r="61.82" gradientTransform="matrix(-0.85, 0, 0, 0.83, 211.97, 6.32)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#fff"/>
        <stop offset="1"/>
        </radialGradient>
        <filter id="luminosity-noclip-2" x="-19.86" y="-8442" width="47.18" height="32766" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-color="#fff" result="bg"/>
        <feBlend in="SourceGraphic" in2="bg"/>
        </filter>
        <mask id="mask-2" x="-19.86" y="-8442" width="47.18" height="32766" maskUnits="userSpaceOnUse">
        <g class="cls-110"/>
        </mask>
        <radialGradient id="radial-gradient-2" cx="222.49" cy="24.8" r="61.82" xlink:href="#radial-gradient"/>
        <linearGradient id="linear-gradient" x1="242.2" y1="12.93" x2="296.53" y2="-33.01" gradientTransform="matrix(-1, 0, 0, 1, 259.93, 0)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#e22013"/>
        <stop offset="1" stop-color="#efa200"/>
        </linearGradient>
        <linearGradient id="linear-gradient-2" x1="237.74" y1="18.73" x2="297.91" y2="-39.93" gradientTransform="matrix(-1, 0, 0, 1, 259.93, 0)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#e22013"/>
        <stop offset="1" stop-color="#f4d099"/>
        </linearGradient>
        <linearGradient id="linear-gradient-3" x1="265.96" y1="-7.71" x2="300.29" y2="-22.02" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-4" x1="307.02" y1="-24.1" x2="279.14" y2="-3.54" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-5" x1="296.46" y1="-38.4" x2="268.53" y2="-17.8" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-6" x1="301.09" y1="-31.87" x2="273.37" y2="-11.43" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-7" x1="303.09" y1="-29.22" x2="275.39" y2="-8.78" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-8" x1="300.25" y1="-33.06" x2="272.46" y2="-12.56" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-9" x1="294.97" y1="-32.84" x2="309.3" y2="-32.84" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-10" x1="293.51" y1="-20.52" x2="298.97" y2="-20.52" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-11" x1="274.82" y1="-4.14" x2="253.38" y2="15.46" xlink:href="#linear-gradient"/>
        <filter id="luminosity-noclip-3" x="-35.27" y="-20.29" width="62.59" height="43.03" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-color="#fff" result="bg"/>
        <feBlend in="SourceGraphic" in2="bg"/>
        </filter>
        <mask id="mask-3" x="15.55" y="38.71" width="62.59" height="43.03" maskUnits="userSpaceOnUse">
        <g class="cls-109">
            <g transform="translate(50.82 59)">
            <g class="cls-1">
                <path class="cls-2" d="M-29.9-10.65a27.69,27.69,0,0,1,4.55-.22,19.44,19.44,0,0,1,5,1c.14.05.19-.09.11-.18a7.71,7.71,0,0,0-2.72-2,14.69,14.69,0,0,1,6.26,2.47A9.37,9.37,0,0,1-13.81-5.9c.66,1.5,1.08,3.09,1.74,4.59,0,.11.23.13.23,0a16.68,16.68,0,0,0-1.27-5.44,40.48,40.48,0,0,1,2.4,5c.61,1.35,1.28,2.69,2,4A21.57,21.57,0,0,0-6.2,6a4.78,4.78,0,0,0,3,1.91.09.09,0,0,0,.11-.13A31,31,0,0,0-5.56,2.63,24.2,24.2,0,0,0,.79,9.39C1,9.5,1,9.3.9,9.2-1.65,7.16-2.73,3.78-4,.91q-1.1-2.55-2.33-5c1.35,1.3,2.65,2.63,3.92,4A29.67,29.67,0,0,0,2.56,4.36C3.78,5.17,5.45,6.08,7,5.8c1.69-.31,1.37-2.19.87-3.39A28,28,0,0,0,4.61-2.45c1,.7,1.92,1.59,2.87,2.37A7.17,7.17,0,0,0,10.8,1.77c.09,0,.13-.05.11-.13a5,5,0,0,0-.28-.93c1.17.83,2.23,2,3.71,2.14a.09.09,0,0,0,.11,0,1.69,1.69,0,0,0,.17-.36c.5-1.36-.15-3-.75-4.18a26.93,26.93,0,0,0-2.55-4,28.57,28.57,0,0,0-9-7.45,33.65,33.65,0,0,1,8.72,3.89,21,21,0,0,1,4.07,3.57c1.15,1.3,2.2,2.67,3.33,4,.06.06.22.08.21-.05-.54-3.93-3.32-7.3-6.67-9.47a21.26,21.26,0,0,1,9.89,9.4c.05.09.26.13.23,0a13.29,13.29,0,0,0-6.83-9.71,8.55,8.55,0,0,1,5.76,3.74c.08.13.3.08.21-.08-1.91-3.32-5.34-5-8.72-6.52A23.7,23.7,0,0,1,8-16.9,4.71,4.71,0,0,1,5.87-20c0-.08,0-.16.07-.24a2.84,2.84,0,0,0,.48,1.2,7,7,0,0,0,2.43,2,19.55,19.55,0,0,0,1.8.83,46.67,46.67,0,0,0,6,2.74A19.09,19.09,0,0,1,22.48-9.7a15.61,15.61,0,0,1,3.13,4.47c2.24,4.88,2.31,12.06,0,16.77C23,17,17.76,20.57,11.64,22-3.87,25.67-14,14.81-16.95,3.65c-.51-1.94-.58-3.95-1-5.91,0-.24-.1-.47-.16-.7l-.17-.23a8.57,8.57,0,0,0-.48-1.4A6.62,6.62,0,0,0-20.5-6.94a3.18,3.18,0,0,1,1,.53A5,5,0,0,1-18.31-5.2a7.4,7.4,0,0,0-1.05-1.36c-1.51-1.53-3.9-2-6-2.24s-4.18.07-6.24-.43a12.69,12.69,0,0,1-3.7-1.6A15.34,15.34,0,0,0-29.9-10.65Z"/>
            </g>
            </g>
        </g>
        </mask>
        <radialGradient id="radial-gradient-3" cx="222.49" cy="24.8" r="61.82" gradientTransform="matrix(-0.85, 0, 0, 0.83, 211.97, 6.32)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#e8a192"/>
        <stop offset="1" stop-color="#efa200"/>
        </radialGradient>
        <filter id="luminosity-noclip-4" x="-19.86" y="-13.57" width="47.18" height="36.3" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-color="#fff" result="bg"/>
        <feBlend in="SourceGraphic" in2="bg"/>
        </filter>
        <mask id="mask-4" x="30.96" y="45.43" width="47.18" height="36.3" maskUnits="userSpaceOnUse">
        <g class="cls-108">
            <g transform="translate(50.82 59)">
            <g class="cls-3">
                <path class="cls-4" d="M-16.41-.37c.7,2.39,1.28,5,2.92,6.91,0,0,.11,0,.1,0-.31-1.75-1.11-3.41-1.1-5.2,1.31,2.73,1.82,6,4.21,8.09a.05.05,0,0,0,.09,0c-.24-1.89-1.5-3.58-1.33-5.52,1.4,3.19,2,7.37,5.35,9.2,0,0,.08,0,.07-.05-.22-.88-.91-1.63-1-2.54A20.38,20.38,0,0,1-5,12.42c.83.82,1.66,1.65,2.55,2.4C-1,16,1,17.42,2.87,16.62a0,0,0,0,0,0-.06c-.79-2-2.78-3.33-3.36-5.41,1.16.62,2,1.7,3.31,2.12.05,0,.06,0,.05-.08-.41-.86-1.09-1.56-1.53-2.41C4.13,12.45,6.66,15,10,15.34c0,0,.05,0,.05,0,.1-1-.7-2-1.29-2.74A15.76,15.76,0,0,1,6.65,9.68c1.91,1,3.12,3.24,5.38,3.59a.06.06,0,0,0,.06-.07,9.27,9.27,0,0,1-.7-1.78,24.5,24.5,0,0,1,2.92,1.52,3.93,3.93,0,0,0,2.85.52,0,0,0,0,0,0-.06c-.85-3.17-3.87-5.21-4.91-8.3,1.27,1.69,2.48,3.67,4.45,4.61,0,0,.08,0,.06-.05A21.33,21.33,0,0,1,15.72,6.5,20.22,20.22,0,0,0,19.2,10s.08,0,.08,0c0-2.66-1.31-5.16-2.39-7.55a19.93,19.93,0,0,1,3.88,7c0,.06.13.08.11,0-.09-.4,0-.81-.07-1.21A10.31,10.31,0,0,1,22.37,11s.1.06.11,0a12.84,12.84,0,0,0,.12-6.16A26,26,0,0,0,20.46-.83,21.63,21.63,0,0,1,23.58,2.6s.11,0,.1,0a7.44,7.44,0,0,0-.52-1.9,7.84,7.84,0,0,1,1.71,2.5s.1.06.11,0C25.47,0,24.79-4,22.13-6.13A9.54,9.54,0,0,1,24.4-4.19s.08,0,.08,0c-.18-2.37-1.87-4.36-3.55-5.93a44.61,44.61,0,0,0-4.31-3.42l.05,0A19.09,19.09,0,0,1,22.48-9.7a15.61,15.61,0,0,1,3.13,4.47c2.24,4.88,2.31,12.06,0,16.77C23,17,17.76,20.57,11.64,22-3.87,25.67-14,14.81-16.95,3.65c-.51-1.94-.58-3.95-1-5.91,0-.24-.1-.47-.16-.7l-.17-.23a8.57,8.57,0,0,0-.48-1.4,6.27,6.27,0,0,0-1.11-1.72C-17.92-4.92-17.07-2.59-16.41-.37Z"/>
            </g>
            </g>
        </g>
        </mask>
        <radialGradient id="radial-gradient-4" cx="222.49" cy="24.8" r="61.82" xlink:href="#radial-gradient-3"/>
        <linearGradient id="linear-gradient-12" x1="262.34" y1="-11.78" x2="304.67" y2="-44.94" gradientTransform="matrix(-1, 0, 0, 1, 259.93, 0)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#e22013"/>
        <stop offset="1" stop-color="#d59900"/>
        </linearGradient>
        <linearGradient id="linear-gradient-13" x1="279.06" y1="-26.96" x2="283.5" y2="-26.96" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-14" x1="285.48" y1="-8.72" x2="309.32" y2="-27.39" xlink:href="#linear-gradient-12"/>
        <linearGradient id="linear-gradient-15" x1="302.71" y1="-23.13" x2="304.14" y2="-23.13" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-16" x1="284.3" y1="0.36" x2="308.14" y2="-18.3" xlink:href="#linear-gradient-12"/>
        <linearGradient id="linear-gradient-17" x1="301.55" y1="-14.12" x2="303.02" y2="-14.12" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-18" x1="278.36" y1="0.7" x2="307.02" y2="-21.75" xlink:href="#linear-gradient-12"/>
        <linearGradient id="linear-gradient-19" x1="282.8" y1="-3.63" x2="284.64" y2="-3.63" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-20" x1="229.06" y1="4.97" x2="271.63" y2="-26.97" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-21" x1="232.65" y1="9.66" x2="275.17" y2="-22.23" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-22" x1="237.74" y1="16.53" x2="280.91" y2="-15.85" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-23" x1="243.66" y1="24.35" x2="286.61" y2="-7.86" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-24" x1="244.75" y1="25.35" x2="287.3" y2="-6.57" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-25" x1="244.68" y1="25.5" x2="287.37" y2="-6.51" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-26" x1="242.01" y1="-7.23" x2="227.45" y2="6.93" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-27" x1="246.34" y1="-2.68" x2="231.88" y2="11.39" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-28" x1="259.27" y1="10.09" x2="244.54" y2="24.42" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-29" x1="304.72" y1="-26.87" x2="277.01" y2="-6.44" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-30" x1="275.25" y1="-3.65" x2="253.85" y2="15.91" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-31" x1="263.46" y1="-16.45" x2="241.97" y2="3.19" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-32" x1="261.26" y1="-18.72" x2="239.86" y2="0.84" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-33" x1="266.76" y1="-12.87" x2="245.29" y2="6.75" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-34" x1="271.01" y1="-8.26" x2="249.6" y2="11.31" xlink:href="#linear-gradient"/>
        <linearGradient id="linear-gradient-35" x1="258.87" y1="-32.68" x2="220.02" y2="9" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-36" x1="289.2" y1="-4.76" x2="250.37" y2="36.9" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-37" x1="290.83" y1="-3.28" x2="251.99" y2="38.39" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-38" x1="236.81" y1="-1.1" x2="228.25" y2="6.57" xlink:href="#linear-gradient-2"/>
        <linearGradient id="linear-gradient-39" x1="260.2" y1="-31.14" x2="221.56" y2="10.31" xlink:href="#linear-gradient-2"/>
        <radialGradient id="radial-gradient-5" cx="-2764.56" cy="2770.95" r="0.99" gradientTransform="matrix(153.62, 0, 0, -158.93, 424950.25, 440571.67)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#ff6900"/>
        <stop offset="0.04" stop-color="#ff6701"/>
        <stop offset="1" stop-color="#fc3a14"/>
        </radialGradient>
        <linearGradient id="Dégradé_sans_nom_93" data-name="Dégradé sans nom 93" x1="144.11" y1="135.68" x2="170.63" y2="135.68" gradientTransform="translate(6.65 -7.36) rotate(1.71)" gradientUnits="userSpaceOnUse">
        <stop offset="0"/>
        <stop offset="0.91" stop-color="blue"/>
        </linearGradient>
        <linearGradient id="Dégradé_sans_nom_93-2" x1="169.4" y1="136.3" x2="189.42" y2="136.3" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-3" x1="187.69" y1="134.79" x2="206.35" y2="134.79" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-4" x1="205.05" y1="132.5" x2="221.28" y2="132.5" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-5" x1="222.4" y1="130.6" x2="239.84" y2="130.6" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-6" x1="240.04" y1="128.46" x2="263.47" y2="128.46" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-7" x1="262.04" y1="126.15" x2="282.06" y2="126.15" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-8" x1="286.49" y1="123.09" x2="305.04" y2="123.09" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-9" x1="306.07" y1="121.43" x2="323.51" y2="121.43" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-10" x1="114.6" y1="178.94" x2="131.34" y2="178.94" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-11" x1="131.92" y1="176.6" x2="151.88" y2="176.6" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-12" x1="151.33" y1="174.6" x2="168.77" y2="174.6" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-13" x1="169.79" y1="173.24" x2="190.06" y2="173.24" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-14" x1="187.97" y1="170.52" x2="205.1" y2="170.52" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-15" x1="206.8" y1="168.52" x2="224.23" y2="168.52" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-16" x1="223.08" y1="166.25" x2="241.63" y2="166.25" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-17" x1="242.26" y1="159.22" x2="247.46" y2="159.22" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-18" x1="249.14" y1="163.9" x2="265.38" y2="163.9" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-19" x1="272.51" y1="161.29" x2="288.83" y2="161.29" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-20" x1="288.63" y1="159.94" x2="299.33" y2="159.94" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-21" x1="298.8" y1="158.31" x2="319.06" y2="158.31" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-22" x1="316.78" y1="155.99" x2="335.33" y2="155.99" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-23" x1="335.68" y1="154.28" x2="355.64" y2="154.28" xlink:href="#Dégradé_sans_nom_93"/>
        <linearGradient id="Dégradé_sans_nom_93-24" x1="355" y1="152.3" x2="371.24" y2="152.3" xlink:href="#Dégradé_sans_nom_93"/>
    </defs>
    <g class="cls-5">
        <g id="OBJECTS">

            <g id="combattant">
                <path class="cls-6" d="M175,420.56s-4.23,5.93-4.59,7.91-3.87,4.35-1.76,7.12a39.66,39.66,0,0,0,6,6.33c.7.4,5.28-9.1,5.28-9.1s5.64-1.58,7.05-9.89-7.75-7.12-7.75-7.12Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M168.29,434.48a3.88,3.88,0,0,0,3.6,2.27c2-.26,3.27-2.68,5.18-3.52a2,2,0,0,0-1.39-1.29l-.13.05-.06-.1.19.05c1-.41,2.38,0,3.29-.65a7,7,0,0,0,2.88-4.73,7.51,7.51,0,0,0-1.36-5.53,1.78,1.78,0,0,0-1-.77,1.76,1.76,0,0,0-1.34.56A13.19,13.19,0,0,0,175,425l-2.34-.86a17.87,17.87,0,0,0-2.2,4.34c-.27,1.56-2.51,3.36-2.43,5.4C168.09,434.08,168.19,434.28,168.29,434.48Z" transform="translate(50.82 59)"/>
                <path class="cls-8" d="M170.43,441.92s3.88-7.52,9.52-9.1a121.13,121.13,0,0,1,11.64,10.29l-2.47,4.74a17.82,17.82,0,0,0-8.11-2.77A31.5,31.5,0,0,1,170.43,441.92Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M190.53,488.6s-7.76,4.74-7.76,5.14,10.23-2,10.23-2l-.71-3.56Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M209.57,488.6s7.75,4.74,7.75,5.14-10.22-2-10.22-2l.7-3.56Z" transform="translate(50.82 59)"/>
                <path class="cls-8" d="M197.23,464.07s1.76,1.58-.35,6.33-4.59,12.66-3.88,17.8l-1.41,2-2.12-1.58s.71-14.24,1.06-18.2a32.44,32.44,0,0,1,.7-5.14l1.42-1.58Z" transform="translate(50.82 59)"/>
                <path class="cls-9" d="M198.48,467" transform="translate(50.82 59)"/>
                <polygon class="cls-10" points="250.87 457.65 243.29 457.61 243.52 463.55 250.87 467.3 258.21 463.55 258.45 457.61 250.87 457.65"/>
                <path class="cls-6" d="M214.55,427.42s3.61,6.43,5.16,7.45,2.4,5.53,5.42,4.2a36.31,36.31,0,0,0,7.29-4.29c.57-.62-5.93-8.58-5.93-8.58a12.38,12.38,0,0,0-6-10.71c-6.54-4.21-8.54,5.89-8.54,5.89Z" transform="translate(50.82 59)"/>
                <path class="cls-8" d="M220.85,410.27l5.64,10.29-1.41,2s-3.53-5.94-8.11-4.35L215.91,415Z" transform="translate(50.82 59)"/>
                <path class="cls-8" d="M179.25,410.27l-5.64,10.29,1.41,2s3.52-5.94,8.11-4.35l1.05-3.17Z" transform="translate(50.82 59)"/>
                <polygon class="cls-8" points="233.59 462.15 229.72 465.32 236.41 474.02 237.82 472.83 233.59 462.15"/>
                <polygon class="cls-8" points="231.48 466.11 228.66 469.67 235.35 475.21 236.41 474.02 231.48 466.11"/>
                <path class="cls-11" d="M179.67,408.35l-1.83,2.32,6.11,5.05C184,413.29,181.46,410.21,179.67,408.35Z" transform="translate(50.82 59)"/>
                <path class="cls-11" d="M184.82,410.64a48.58,48.58,0,0,0-3.53-6.27l-2.39,1.95,6.28,8.16,0,0a2.87,2.87,0,0,0,.27-1.94A7.11,7.11,0,0,0,184.82,410.64Z" transform="translate(50.82 59)"/>
                <polygon class="cls-8" points="236.06 459.78 239.59 472.44 237.82 472.83 232.18 461.36 236.06 459.78"/>
                <path class="cls-11" d="M184.38,401.13l-3,1.23,5.09,10.34v0a4.32,4.32,0,0,0,.09-2.62A77.3,77.3,0,0,0,184.38,401.13Z" transform="translate(50.82 59)"/>
                <path class="cls-11" d="M178.24,412.12l-4.63,8.44,1,1.44a12.56,12.56,0,0,1,5-4.36c.66-.3,1.43-.66,1.57-1.44a2.24,2.24,0,0,0-.53-1.59A11.14,11.14,0,0,0,178.24,412.12Z" transform="translate(50.82 59)"/>
                <path class="cls-12" d="M214.15,407.51l-4.94-.8.36-3.16L204.63,402l-1.06,3.95h0a3,3,0,0,0-3.18,1.7l-.34.68-.35-.68a3,3,0,0,0-3.18-1.7h0L195.47,402l-4.94,1.58.35,3.16-4.93.8s-4.59,11.07-4.23,11.86S191.94,436,191.94,436a28.46,28.46,0,0,0,16.22,0s9.87-15.83,10.22-16.62S214.15,407.51,214.15,407.51Z" transform="translate(50.82 59)"/>
                <path class="cls-8" d="M218.38,419.37s0,0,0-.06c-.38-.15-.77-.28-1.17-.41-11.23-3.53-24.27-2.72-35.5.49.44.91,10.22,16.6,10.22,16.6a28.46,28.46,0,0,0,16.22,0S218,420.16,218.38,419.37Z" transform="translate(50.82 59)"/>
                <path class="cls-13" d="M193.35,389.11a2,2,0,1,1-1.94-2.17A2.07,2.07,0,0,1,193.35,389.11Z" transform="translate(50.82 59)"/>
                <path class="cls-13" d="M206.75,389.11a2,2,0,1,0,1.94-2.17A2.07,2.07,0,0,0,206.75,389.11Z" transform="translate(50.82 59)"/>
                <path class="cls-14" d="M207.8,382.59c-.35-2.38-1.41-3.17-7.75-3.17s-7.4.79-7.76,3.17.36,15.42.71,15.82,3.88,2.77,7.05,2.77,6.7-2.38,7-2.77S208.16,385,207.8,382.59Z" transform="translate(50.82 59)"/>
                <path class="cls-13" d="M200.05,379.42c-6.35,0-7.4.79-7.76,3.17a85.44,85.44,0,0,0,.16,9.9l.88.48a11.27,11.27,0,0,1,1.78-10.79,4.86,4.86,0,0,0,4.82,1.5c1.47-.4,2.71-1.5,4.13-2.11a3.56,3.56,0,0,1,3.58.22C207.12,380,205.62,379.42,200.05,379.42Z" transform="translate(50.82 59)"/>
                <path class="cls-10" d="M199.54,385.83s-.64,6.53-.72,7.3c-.19,1.83,2.5.56,2.39.57l-1.51-1Z" transform="translate(50.82 59)"/>
                <polygon class="cls-8" points="270.26 466.11 273.08 469.67 266.38 475.21 265.32 474.02 270.26 466.11"/>
                <polygon class="cls-8" points="268.14 462.15 272.02 465.32 265.32 474.02 263.91 472.83 268.14 462.15"/>
                <polygon class="cls-8" points="265.68 459.78 262.15 472.44 263.91 472.83 269.55 461.36 265.68 459.78"/>
                <path class="cls-15" d="M206.39,438.36l-6.34,1.19-6.35-1.19s-3.88,15.43-2.11,21.36l1.06,4,4.58.39s2.11-14.24,2.11-17h1.41c0,2.77,2.12,17,2.12,17l4.58-.39,1.06-4C210.27,453.79,206.39,438.36,206.39,438.36Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M193,438.76a129.62,129.62,0,0,0-2.47,15s6.7-4.35,7.76-8.71.35-6.32.35-6.32Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M207.1,438.76a132,132,0,0,1,2.47,15s-6.7-4.35-7.76-8.71-.35-6.32-.35-6.32Z" transform="translate(50.82 59)"/>
                <path class="cls-8" d="M202.87,464.07s-1.76,1.58.35,6.33,4.58,12.66,3.88,17.8l1.41,2,2.12-1.58s-.71-14.24-1.06-18.2a32.87,32.87,0,0,0-.71-5.14l-1.41-1.58Z" transform="translate(50.82 59)"/>
                <path class="cls-11" d="M209.15,466.79a5.29,5.29,0,0,1-4.19-2.9l-2.09.18a2.37,2.37,0,0,0-.58,1.21l5.47,2c.52,7.34,1.23,14.79,1.79,22.15l1.08-.8s-.71-14.24-1.06-18.2C209.42,468.75,209.27,467.59,209.15,466.79Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M190.94,466.79c1.8-.31,3.58-1.28,4.19-2.9l2.1.18a2.37,2.37,0,0,1,.58,1.21l-5.47,2c-.52,7.34-1.23,14.79-1.79,22.15l-1.08-.8s.71-14.24,1.06-18.2C190.68,468.75,190.82,467.59,190.94,466.79Z" transform="translate(50.82 59)"/>
                <path class="cls-11" d="M208.86,465.26s2.12-2.77-1.41-7.91C207.45,457.35,202.87,465.65,208.86,465.26Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M201.62,467" transform="translate(50.82 59)"/>
                <ellipse class="cls-16" cx="247.52" cy="446.53" rx="0.88" ry="0.99"/>
                <path class="cls-16" d="M193.39,383.11a5.16,5.16,0,0,1,3.2.92,2.6,2.6,0,0,0,1,.53,1.17,1.17,0,0,0,1.2-1.23c-.16.22-.57.08-.81,0a2.9,2.9,0,0,1-.62-.5,3,3,0,0,0-2.18-.79A2.56,2.56,0,0,0,193.39,383.11Z" transform="translate(50.82 59)"/>
                <ellipse class="cls-16" cx="254.22" cy="446.53" rx="0.88" ry="0.99"/>
                <path class="cls-16" d="M206.71,383.11a5.19,5.19,0,0,0-3.21.92,2.5,2.5,0,0,1-1,.53,1.17,1.17,0,0,1-1.2-1.23c.16.22.57.08.8,0a2.67,2.67,0,0,0,.62-.5,3,3,0,0,1,2.19-.79A2.58,2.58,0,0,1,206.71,383.11Z" transform="translate(50.82 59)"/>
                <path class="cls-16" d="M191.06,380c0,1.19,1.06,11.08,1.06,11.08s.35,6.72.35,7.52,3.83,3.36,7.58,3.36,7.58-2.57,7.58-3.36.35-7.52.35-7.52S209,381.2,209,380s-4.23-1.58-4.23-1.58l-.53,2c3.92,0,3.4,7.51,3.12,9.44a24.37,24.37,0,0,1-1.55,4.8l-4.5-.75a7.63,7.63,0,0,0-2.61,0l-4.49.75a23.67,23.67,0,0,1-1.55-4.8c-.28-1.93-.8-9.44,3.12-9.44l-.53-2S191.06,378.83,191.06,380Zm3.5,18.25.23-2.72a24.58,24.58,0,0,1,5.26-.89,24.72,24.72,0,0,1,5.26.89l.22,2.72a13.46,13.46,0,0,1-3.34,1.38c-.49-2-2.14-1.63-2.14-1.63s-1.65-.39-2.14,1.63A13.48,13.48,0,0,1,194.56,398.26Z" transform="translate(50.82 59)"/>
                <path class="cls-6" d="M191.59,443.11a5.54,5.54,0,0,1,.56,1.65c.06.7-1,8.25-1.3,8.38a6.62,6.62,0,0,1-2.67-1.05c0-.21.47-2.39.7-2.32s.59,1.25.59,1.25l.45-.92s-1.24-1.39-.8-2.25Z" transform="translate(50.82 59)"/>
                <path class="cls-6" d="M208.51,443.11a5.54,5.54,0,0,0-.56,1.65c-.07.7,1,8.25,1.3,8.38a6.62,6.62,0,0,0,2.67-1.05c0-.21-.47-2.39-.71-2.32s-.58,1.25-.58,1.25l-.45-.92s1.24-1.39.8-2.25Z" transform="translate(50.82 59)"/>
                <path class="cls-16" d="M200,382.4a6.43,6.43,0,0,0,5.88-6.22,4.33,4.33,0,0,0-2.2-3.9,2.59,2.59,0,0,1-.76,2.67,1.86,1.86,0,0,0-.19-1.71,1.39,1.39,0,0,0-1.39-.67,1.55,1.55,0,0,0-1.15,1.09.62.62,0,0,1,1.07-.06,1.18,1.18,0,0,1-.11,1.35,1.42,1.42,0,0,1-1.21.45,3.45,3.45,0,0,1-1.25-.43,7,7,0,0,0-1.22-.55,2.67,2.67,0,0,0-2.08.29,3.89,3.89,0,0,0-1.59,3.88,4.94,4.94,0,0,0,2.93,3.29A5.89,5.89,0,0,0,200,382.4Z" transform="translate(50.82 59)"/>
                <path class="cls-6" d="M195.77,449.32a10.6,10.6,0,0,0,2.52-4.24,14.93,14.93,0,0,0,.47-5.8l-1-.13Z" transform="translate(50.82 59)"/>
                <path class="cls-6" d="M202.32,439.15l-1,.13a14.72,14.72,0,0,0,.47,5.8,10.6,10.6,0,0,0,2.52,4.24Z" transform="translate(50.82 59)"/>
                <polygon class="cls-8" points="252.63 498.15 250.87 498.55 249.1 498.15 246.99 515.95 250.87 521.49 254.75 515.95 252.63 498.15"/>
                <path class="cls-8" d="M193.25,436l-.25,0v2.77a31.85,31.85,0,0,0,14.1,0V436l-.25,0A40.85,40.85,0,0,1,193.25,436Z" transform="translate(50.82 59)"/>
                <path class="cls-11" d="M200.05,439.55a30.63,30.63,0,0,0,7-.79v-1.9a16.69,16.69,0,0,1-9.28,1,5.4,5.4,0,0,0-2.36-.13,2,2,0,0,0-1.33,1.3A36.2,36.2,0,0,0,200.05,439.55Z" transform="translate(50.82 59)"/>
                <path class="cls-12" d="M191.23,465.26s-2.11-2.77,1.42-7.91C192.65,457.35,197.23,465.65,191.23,465.26Z" transform="translate(50.82 59)"/>
                <path class="cls-17" d="M169.65,433.67a1.41,1.41,0,0,1-.7-1.82c.35-1.17,1.24-1.85,1.66-3a4.06,4.06,0,0,1,2.57-2.63,1.63,1.63,0,0,1,1.15,0,1.66,1.66,0,0,1,.71,1.56,3.81,3.81,0,0,1-.42,1.55,10.29,10.29,0,0,1-3.19,4.16A1.83,1.83,0,0,1,169.65,433.67Z" transform="translate(50.82 59)"/>
                <path class="cls-12" d="M220.42,408.35l1.84,2.32-6.11,5.05C216.15,413.29,218.64,410.21,220.42,408.35Z" transform="translate(50.82 59)"/>
                <path class="cls-12" d="M215.28,410.64a47.42,47.42,0,0,1,3.53-6.27l2.39,1.95-6.28,8.16,0,0a2.87,2.87,0,0,1-.27-1.94A7.11,7.11,0,0,1,215.28,410.64Z" transform="translate(50.82 59)"/>
                <path class="cls-12" d="M215.71,401.13l3,1.23-5.08,10.34,0,0a4.32,4.32,0,0,1-.08-2.62A79.38,79.38,0,0,1,215.71,401.13Z" transform="translate(50.82 59)"/>
                <path class="cls-12" d="M221.86,412.12l4.63,8.44-1,1.44a12.43,12.43,0,0,0-5-4.36c-.65-.3-1.42-.66-1.56-1.44a2.24,2.24,0,0,1,.53-1.59A10.93,10.93,0,0,1,221.86,412.12Z" transform="translate(50.82 59)"/>
                <path class="cls-18" d="M214.15,407.51l-4.94-.8.36-3.16L204.63,402l-1.06,3.95a3,3,0,0,0-3.18,1.7l-.34.68v28.87a28.28,28.28,0,0,0,8.11-1.18s9.87-15.83,10.22-16.62S214.15,407.51,214.15,407.51Z" transform="translate(50.82 59)"/>
                <path class="cls-11" d="M217.22,418.9a54.34,54.34,0,0,0-17.17-2.29v20.56a28.28,28.28,0,0,0,8.11-1.18s9.87-15.83,10.22-16.62c0,0,0,0,0-.06C218,419.16,217.62,419,217.22,418.9Z" transform="translate(50.82 59)"/>
                <path class="cls-11" d="M203.93,457,202,441l-2.85.86c-.16,4.64-.54,9.44-.7,14.08a13.94,13.94,0,0,0,1.74,6.29Z" transform="translate(50.82 59)"/>
                <path class="cls-11" d="M190.52,445.16l-18.42-5.91a20.47,20.47,0,0,1,5-5.08A139,139,0,0,1,191,444.23Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M225.89,429.39a1.9,1.9,0,0,0-1.54,1.05v.16l-.1,0a1.14,1.14,0,0,1,.1-.19c0-1.2.75-2.53.53-3.7a6.88,6.88,0,0,0-3-4.6,5.54,5.54,0,0,0-2.47-.8,12.36,12.36,0,0,0-3.1,4.23,15.55,15.55,0,0,0,2,3.35l-1.49,2.2a16.64,16.64,0,0,0,2.94,3.75c1.22.8,2,3.77,3.76,4.34a4.2,4.2,0,0,0,3.69-3.18C227.61,433.78,226,431.69,225.89,429.39Z" transform="translate(50.82 59)"/>
                <path class="cls-17" d="M223.83,437.4a1.13,1.13,0,0,1-1.76.15c-.87-.76-1.15-1.93-2-2.74a4.6,4.6,0,0,1-1.38-3.59,2.31,2.31,0,0,1,.41-1.21,1.32,1.32,0,0,1,1.55-.23,3.11,3.11,0,0,1,1.16,1,10.78,10.78,0,0,1,2.46,4.74A2.45,2.45,0,0,1,223.83,437.4Z" transform="translate(50.82 59)"/>
                <path class="cls-18" d="M207.74,462.46a4.44,4.44,0,0,0,1.54,1.88c.36-1.17.5-3.43-1.62-6.68C207.52,459.34,207.09,460.92,207.74,462.46Z" transform="translate(50.82 59)"/>
                <path class="cls-19" d="M216.12,416.92c-1-2.38-1.23-5.62-3.07-7.26a8.19,8.19,0,0,0-5.7-2.21c.61,3,1.23,5.26,1.84,8.29C211.62,416,213.69,416.63,216.12,416.92Z" transform="translate(50.82 59)"/>
                <path class="cls-8" d="M200.58,418a48.17,48.17,0,0,1,16.12,1.93c-1,2.12-2.21,4.55-3.23,6.66a4.13,4.13,0,0,0-1.26-3.5,8.52,8.52,0,0,0-3-1.49C206.26,420.54,203.54,419,200.58,418Z" transform="translate(50.82 59)"/>
                <path class="cls-19" d="M191.24,404l4.2-1.5c.13,1,.17,2.16.3,3.14C194,406.25,192.6,405.3,191.24,404Z" transform="translate(50.82 59)"/>
                <path class="cls-19" d="M198.74,414.26a12.66,12.66,0,0,0,.4-3.87,4,4,0,0,0-1.78-3.17,4.25,4.25,0,0,0-2.26-.34,24.52,24.52,0,0,0-6.5,1.2,15.59,15.59,0,0,1,5.4-.2,6.06,6.06,0,0,1,4.53,3.18A4.67,4.67,0,0,1,198.74,414.26Z" transform="translate(50.82 59)"/>
                <path class="cls-13" d="M200.05,394.65a17.41,17.41,0,0,0-3,.38h0c0,1.44,6,1.44,6,0h0A17.58,17.58,0,0,0,200.05,394.65Z" transform="translate(50.82 59)"/>
                <path class="cls-20" d="M196.49,380.46a2.31,2.31,0,0,1,.36-3,4,4,0,0,1,2.82-.88,10.61,10.61,0,0,0,3-.18,2.74,2.74,0,0,0,2-2.19,6,6,0,0,1-1.93,6.22C201,381.81,197.93,382.16,196.49,380.46Z" transform="translate(50.82 59)"/>
            </g>

            <g id="avant-bras">
                <path class="cls-12" d="M224.29,440.31s1.23-8.18,6.29-12.34a118.82,118.82,0,0,1,15.16,2.65L245,435.8a21.55,21.55,0,0,0-9,1.79A39.6,39.6,0,0,1,224.29,440.31Z" transform="translate(50.82 59)"/>
                <path class="cls-18" d="M244.9,433.38l-17.51,3.07a20.6,20.6,0,0,1,2.38-6.35,118.67,118.67,0,0,1,15.2,2.3Z" transform="translate(50.82 59)"/>
                <path class="cls-6" d="M244,431.2a7.06,7.06,0,0,1,2.61-2.77c1.47-.64,2.74-3.88,3.39-3.56s-.83,3.51-.83,3.51,3.32-1.39,2.7,1.08,1.16,1.67.21,3.75.14,3-1.41,3.47-5.28-.43-5.28-.43Z" transform="translate(50.82 59)"/>
            </g>

            <g id="epee">
                <rect class="cls-21" x="50.53" y="22.01" width="2.1" height="100.32"/>
                <rect class="cls-22" x="51.58" y="22.68" width="0.7" height="98.98"/>
                <g>
                <path class="cls-6" d="M.76-59a49.24,49.24,0,0,0-1.82,9.65C-1.76-43-1-43,.76-43Z" transform="translate(50.82 59)"/>
                <path class="cls-6" d="M.76-59a48.29,48.29,0,0,1,1.82,9.65C3.29-43,2.53-43,.76-43Z" transform="translate(50.82 59)"/>
                <path class="cls-6" d="M.76-59v22H-1.35c-.59,0-1.53-1-1.53-1l2.47.53Z" transform="translate(50.82 59)"/>
                <path class="cls-7" d="M.76-59v22H2.88c.58,0,1.52-1,1.52-1l-2.46.53Z" transform="translate(50.82 59)"/>
                </g>
            </g>

            <g id="asteroide">
                <g>
                <path class="cls-23" d="M-50.81-32.37c1.94,3.09,6.88,1.38,10,1.89a.1.1,0,0,0,.11-.14,4.84,4.84,0,0,0-.92-1.48,11,11,0,0,0-4.68-2.52,13.47,13.47,0,0,1-3.39-1.57A4.44,4.44,0,0,1-50-41.32a.22.22,0,0,1,0,.08,3.24,3.24,0,0,0,.64,2.14,6.65,6.65,0,0,0,3.63,2.4,7.43,7.43,0,0,0,2.34.32,8.65,8.65,0,0,0,2.18-.46,8.46,8.46,0,0,1,4.62-.26A3.59,3.59,0,0,1-34-33.54c0,.11.2.22.25.08a5.27,5.27,0,0,0,.19-2.82c.26.31.52.62.76.94a35.9,35.9,0,0,1,.5,4.86c.15,2.07.38,4.43,1.67,6.16.35.46,1,1.29,1.7,1.38q-.21-.43-.39-.87a4.62,4.62,0,0,1,0-2.22,3.32,3.32,0,0,1,.22-.65c.52,3.39,3.62,6,7,7.26a13.12,13.12,0,0,0,1.28.51,19,19,0,0,0,7.3.89,17.25,17.25,0,0,0,3.65-.58c1-.27,2-.51,2.95-.7a19.37,19.37,0,0,1,2.25-.33,11.17,11.17,0,0,1,4.21.52c.15.06.2-.13.08-.21a7.82,7.82,0,0,1-2.18-1.79h0a12.61,12.61,0,0,0,2.73.49,10.16,10.16,0,0,1,2.47.67,10.08,10.08,0,0,1,3,1.92,4.81,4.81,0,0,1,.25-2.27,2.84,2.84,0,0,0,.48,1.2,7,7,0,0,0,2.43,2,19.55,19.55,0,0,0,1.8.83,46.67,46.67,0,0,0,6,2.74A19.09,19.09,0,0,1,22.48-9.7a15.61,15.61,0,0,1,3.13,4.47c2.24,4.88,2.31,12.06,0,16.77C23,17,17.76,20.57,11.64,22-3.87,25.67-14,14.81-16.95,3.65c-.51-1.94-.58-3.95-1-5.91,0-.24-.1-.47-.16-.7l-.17-.23a8.57,8.57,0,0,0-.48-1.4A6.62,6.62,0,0,0-20.5-6.94a3.18,3.18,0,0,1,1,.53A5,5,0,0,1-18.31-5.2a7.4,7.4,0,0,0-1.05-1.36c-1.51-1.53-3.9-2-6-2.24s-4.18.07-6.24-.43A12.87,12.87,0,0,1-36.84-12l-.08-.07a7.54,7.54,0,0,1-.95-1.15,15.92,15.92,0,0,1-2-4.09,0,0,0,0,0,0,0c-.12-.36-.23-.72-.34-1.08,0,0,0-.09,0-.14a6.38,6.38,0,0,1,0-1.38c.06-.56.19-1.11.24-1.66a3.14,3.14,0,0,0-.35-1.75,5.4,5.4,0,0,1,.75,1c.65,1.19.43,2.68,1.27,3.77.06.08.22.08.2,0l0-.43c-.24-3.09-.28-6.83-3.51-8.42a8.66,8.66,0,0,0-3.08-.84,16.42,16.42,0,0,1-3.43-.42,5.57,5.57,0,0,1-1.06-.46l-.28-.25A4.06,4.06,0,0,1-50.81-32.37Z" transform="translate(50.82 59)"/>
                <path class="cls-24" d="M-50.81-32.37c1.94,3.09,6.88,1.38,10,1.89a.1.1,0,0,0,.11-.14,4.84,4.84,0,0,0-.92-1.48,11,11,0,0,0-4.68-2.52,13.47,13.47,0,0,1-3.39-1.57,4.48,4.48,0,0,1-.82-4,5.14,5.14,0,0,0,2.89,4.75,18.12,18.12,0,0,0,4,1.06c.49.17,1,.34,1.43.53A6.38,6.38,0,0,1-39.45-32a3.89,3.89,0,0,1,.63,2.92A8.75,8.75,0,0,0-40-29.64a9.07,9.07,0,0,0-2.73-.54,3.43,3.43,0,0,0-2.5.6c-.09.08,0,.25.13.23l.12,0a7.34,7.34,0,0,1,8.13,6.77,14.83,14.83,0,0,1-.32,3.32,6.3,6.3,0,0,0,0,2.8,5.88,5.88,0,0,1-1-1.12c0-.07-.2-.14-.23,0-.53,2.19,1.66,4,3.34,5a11.81,11.81,0,0,0,6.87,1.71c3,.28,6.32.61,8.55,2.77.06.06.22.09.21-.05a3,3,0,0,0-.2-.89,13,13,0,0,1,3.28,3.72,11.14,11.14,0,0,1,1.12,2.83,7.77,7.77,0,0,1,.25,1.73c0,.53,0,1.07.07,1.6,0,.09.18.23.24.11A3.65,3.65,0,0,0-14.2-1c.76,1.57.56,3.37.48,5.08a11.87,11.87,0,0,0,.09,2.73A7.34,7.34,0,0,0-13,8.56a13.48,13.48,0,0,0,2.29,3.61,3.79,3.79,0,0,0,3.1,1.49c.1,0,.1-.1.07-.17a6.46,6.46,0,0,1-.68-2c0-.08,0-.16,0-.25a10.68,10.68,0,0,0,3.11,3.63A14.53,14.53,0,0,0,1.08,18c.14,0,.13-.13.08-.2-.53-.72-1.22-1.32-1.71-2.06a3.4,3.4,0,0,1-.6-1.38,1.35,1.35,0,0,1,0-.53c.12-.36.46-.1.65,0a.1.1,0,0,0,.14,0,21.09,21.09,0,0,0,5.58,2.46,17.69,17.69,0,0,0,6.83.53,13.74,13.74,0,0,0,6.35-2.27,12.53,12.53,0,0,0,2.45-2.32,14.28,14.28,0,0,0,1.76-3A11,11,0,0,0,23.75,5.4s0,0,0-.07a18.25,18.25,0,0,0,.05-5.57,11,11,0,0,0-1.69-4.21,7.89,7.89,0,0,1,2.2,2.51c.16.56.3,1.12.43,1.68,0,.12.25.18.25,0A4,4,0,0,0,24.58-2a13.54,13.54,0,0,0-1.85-4.08A18,18,0,0,0,18-10.43a29.73,29.73,0,0,0-5.71-2.82,12.94,12.94,0,0,1-3-1.48,8.71,8.71,0,0,1-2.12-2.59c0-.06-.17-.15-.22,0a4.66,4.66,0,0,0,.12,3.1c-1.47-2-3.11-4.34-5.8-4.53,0,0-.08,0-.1,0L1-18.85c-.14,0-.13.13-.08.2A3.55,3.55,0,0,1,1.84-17L.47-17.44a10.79,10.79,0,0,0-3.41-.69,17.07,17.07,0,0,0-3.69.5,34.13,34.13,0,0,1-8.16,1.17,22.65,22.65,0,0,1-7.81-1.49,12.71,12.71,0,0,1-2.88-1.55,4.85,4.85,0,0,1-2-2.33.16.16,0,0,0-.18-.11c-.07,0-.14,0-.12.12a8.35,8.35,0,0,0,1.07,2.33,8.12,8.12,0,0,1-5.48-4.26,10.64,10.64,0,0,1-1.34-3.73c-.18-1.38.13-2.75-.07-4.13,0-.08-.18-.25-.24-.11a6.43,6.43,0,0,1-1,2c0-.22,0-.44.05-.65a6.49,6.49,0,0,0-.76-4.2C-36.72-36.21-39-36-40.73-35.8a13.43,13.43,0,0,1-3.85.07A7.53,7.53,0,0,1-45.82-36a7.47,7.47,0,0,1-3.42-2.88,6.76,6.76,0,0,0,3.51,2.23,7.43,7.43,0,0,0,2.34.32,8.65,8.65,0,0,0,2.18-.46,8.46,8.46,0,0,1,4.62-.26A3.59,3.59,0,0,1-34-33.54c0,.11.2.22.25.08a5.27,5.27,0,0,0,.19-2.82c.26.31.52.62.76.94a35.9,35.9,0,0,1,.5,4.86c.15,2.07.38,4.43,1.67,6.16.35.46,1,1.29,1.7,1.38q-.21-.43-.39-.87a4.62,4.62,0,0,1,0-2.22,3.32,3.32,0,0,1,.22-.65c.52,3.39,3.62,6,7,7.26a13.12,13.12,0,0,0,1.28.51,19,19,0,0,0,7.3.89,17.25,17.25,0,0,0,3.65-.58c1-.27,2-.51,2.95-.7a19.37,19.37,0,0,1,2.25-.33,11.17,11.17,0,0,1,4.21.52c.15.06.2-.13.08-.21a7.82,7.82,0,0,1-2.18-1.79h0a12.61,12.61,0,0,0,2.73.49,10.16,10.16,0,0,1,2.47.67,10.08,10.08,0,0,1,3,1.92,4.81,4.81,0,0,1,.25-2.27,2.84,2.84,0,0,0,.48,1.2,7,7,0,0,0,2.43,2,19.55,19.55,0,0,0,1.8.83,46.67,46.67,0,0,0,6,2.74A19.09,19.09,0,0,1,22.48-9.7a15.61,15.61,0,0,1,3.13,4.47c2.24,4.88,2.31,12.06,0,16.77C23,17,17.76,20.57,11.64,22-3.87,25.67-14,14.81-16.95,3.65c-.51-1.94-.58-3.95-1-5.91,0-.24-.1-.47-.16-.7l-.17-.23a8.57,8.57,0,0,0-.48-1.4A6.62,6.62,0,0,0-20.5-6.94a3.18,3.18,0,0,1,1,.53A5,5,0,0,1-18.31-5.2a7.4,7.4,0,0,0-1.05-1.36c-1.51-1.53-3.9-2-6-2.24s-4.18.07-6.24-.43A12.87,12.87,0,0,1-36.84-12l-.08-.07a7.54,7.54,0,0,1-.95-1.15,15.92,15.92,0,0,1-2-4.09,0,0,0,0,0,0,0c-.12-.36-.23-.72-.34-1.08,0,0,0-.09,0-.14a6.38,6.38,0,0,1,0-1.38c.06-.56.19-1.11.24-1.66a3.14,3.14,0,0,0-.35-1.75,5.4,5.4,0,0,1,.75,1c.65,1.19.43,2.68,1.27,3.77.06.08.22.08.2,0l0-.43c-.24-3.09-.28-6.83-3.51-8.42a8.66,8.66,0,0,0-3.08-.84,16.42,16.42,0,0,1-3.43-.42,5.57,5.57,0,0,1-1.06-.46l-.28-.25A4.06,4.06,0,0,1-50.81-32.37Z" transform="translate(50.82 59)"/>
                <g>
                    <path class="cls-25" d="M6.6-14a9.36,9.36,0,0,0-5.91.61c-2,1-2.53,1.38-3.62,1.86A52.37,52.37,0,0,0-7.39-8.37c-1.53,1-3,1.54-4.23,4.24a2.43,2.43,0,0,1-1,.9,2.1,2.1,0,0,0-.64.5c-.34.48-.1,1.06.57,1.43s.42,1.68.13,1.87-1,.84-.57,1.46-.29,2,1,5.12c.28,1.12-.27,1.41.35,2.3s1.13,2.36,1.78,2.82a10.18,10.18,0,0,1,2.07,2.88c.35.74,4,4.35,6.64,4.55,1.65.45,2.62.29,3.54.91s1.24,0,1.59.49.63.55,1.62.06c.53-.2,4,.55,9-2,4.78-2.47,5.38-3.28,5.66-4a1,1,0,0,1,.54-.5c.33-.17.64-.32.43-1,1.06-1.12,3.88-6.3,3-10,.7-.8.68-2.26-.53-1.86.33-1.2.59-3.35-2.1-7.36a12.53,12.53,0,0,1-.95-2c-.06-.21-.6,0-.77-.64S18.64-10.78,17.49-11s-1.72-.81-1.71-1.39-.31-.45-.71-.18-3.33-1.7-6.42-1.59c-.55-.2-1-1.16-1.25-1S7.15-14,6.6-14Z" transform="translate(50.82 59)"/>
                    <path class="cls-26" d="M3.88-9.68a7.22,7.22,0,0,1,2.93-.1.1.1,0,0,0,.12-.11A4.76,4.76,0,0,1,8-9.79c.36.09.73.39.49.8a1.24,1.24,0,0,1-.62.48,4.59,4.59,0,0,1-2.52.13,3.93,3.93,0,0,1-1.47-.45C3.52-9.06,3.39-9.52,3.88-9.68Z" transform="translate(50.82 59)"/>
                    <path class="cls-26" d="M-5.93,5.26a1.87,1.87,0,0,1,.26,2,1.54,1.54,0,0,1-.93.82A1,1,0,0,1-7.68,7.5a1,1,0,0,0,1.3-.72,4.06,4.06,0,0,0-.68-2.19A5.3,5.3,0,0,0-8.32,2.76a1.31,1.31,0,0,0-.29-.17C-7.82,2.37-6.15,4.88-5.93,5.26Z" transform="translate(50.82 59)"/>
                    <path class="cls-26" d="M-1.89-3.41a4.1,4.1,0,0,0,.43-1.47,3.09,3.09,0,0,1-.37,3.15,3,3,0,0,1-3.4.84A4.82,4.82,0,0,0-2.78-2,3.36,3.36,0,0,0-1.89-3.41Z" transform="translate(50.82 59)"/>
                    <path class="cls-26" d="M2.79,6A3.23,3.23,0,0,0,5,6.36,1.94,1.94,0,0,0,6.63,5.21a1,1,0,0,0,0-.67c.36.19.88.49.78.91A2.32,2.32,0,0,1,6.17,6.73,3.81,3.81,0,0,1,4.89,7,4.26,4.26,0,0,1,2.1,6.38a1.37,1.37,0,0,1-.23-2A2.06,2.06,0,0,0,2.79,6Z" transform="translate(50.82 59)"/>
                    <path class="cls-26" d="M9.67-6.56a2.8,2.8,0,0,1,1.13.5c.23.16.46.34.68.52-.11,0-.1.19,0,.24a4.46,4.46,0,0,1,.55.23c.16.14.32.28.49.41s0,0,.05,0,.05.29-.16.44A1.83,1.83,0,0,1,11-4,2.5,2.5,0,0,1,9-5.63C8.9-6.07,9.1-6.64,9.67-6.56Z" transform="translate(50.82 59)"/>
                    <path class="cls-27" d="M-13.09,2c.42.61-.29,2,1,5.12.28,1.12-.27,1.41.35,2.3s1.13,2.36,1.78,2.82a10.18,10.18,0,0,1,2.07,2.88c.35.74,4,4.35,6.64,4.55,1.65.45,2.62.29,3.54.91s1.24,0,1.59.49.63.55,1.62.06c.53-.2,4,.55,9-2,4.78-2.47,5.38-3.28,5.66-4a1,1,0,0,1,.54-.5c.33-.17.64-.32.43-1,1.06-1.12,3.88-6.3,3-10,.7-.8.68-2.26-.53-1.86.33-1.2.59-3.35-2.1-7.36a12.53,12.53,0,0,1-.95-2c-.06-.21-.6,0-.77-.64S18.64-10.78,17.49-11s-1.72-.81-1.71-1.39-.31-.45-.71-.18-3.33-1.7-6.42-1.59c-.55-.2-1-1.16-1.25-1S7.15-14,6.6-14a9.36,9.36,0,0,0-5.91.61c-2,1-2.53,1.38-3.62,1.86A52.37,52.37,0,0,0-7.39-8.37c-1.53,1-3,1.54-4.23,4.24a2.43,2.43,0,0,1-1,.9,2.1,2.1,0,0,0-.64.5c-.34.48-.1,1.06.57,1.43s.42,1.68.13,1.87S-13.52,1.41-13.09,2Zm2.22-.27c.31-1-.47-2-.5-3s.78-1.24,1.15-1.93A19.8,19.8,0,0,1-9-5.86,12.35,12.35,0,0,1-6.35-8.18,24.13,24.13,0,0,1-3.52-9.92c.77-.42,1.51-.88,2.32-1.23a4.48,4.48,0,0,1,1.29-.39,1,1,0,0,1,.93.29,1.81,1.81,0,0,0,.82.44,4,4,0,0,0,1,0,6.13,6.13,0,0,0,2.2-.68c1.41-.68,3.44-2.16,5-1.3a27.76,27.76,0,0,1,3.12,1.17,5.57,5.57,0,0,1,.86.75,3.49,3.49,0,0,0,1.52,1A3.82,3.82,0,0,1,17.7-8.34,15.55,15.55,0,0,1,19-5.84a2.51,2.51,0,0,1,.22,1.11.33.33,0,0,1-.08.19,6.33,6.33,0,0,0-.47.56A3.59,3.59,0,0,0,18-1.66a11.75,11.75,0,0,0,.57,3.53c.12.37.39,1.11.91,1s.47-1.12,1.05-1.2.8.9.88,1.24a8.52,8.52,0,0,1,.23,2.3A4.88,4.88,0,0,1,20.73,8c-.42.55-1.09.28-1.6,0-.31-.17-.69-.45-1.06-.35A1.14,1.14,0,0,0,17.53,9c.25.47,1,.87.49,1.47-.27.35-.68.51-.9.92a4,4,0,0,0-.28,1.66,1.69,1.69,0,0,1-.19.83c-.17.26-.54.27-.81.26s-.67-.21-1-.18c-.6,0-.91.81-1.12,1.27A4.64,4.64,0,0,1,12,17c-.53.38-1.14.86-1.82.58a4,4,0,0,1-.91-.73,2.42,2.42,0,0,0-1.46-.53c-1.56-.09-2.57,1.43-4,1.7a8.21,8.21,0,0,1-2-.07c-.83-.07-1.66-.15-2.48-.3-1.09-.2-3.25-.73-3.15-2.2,0-.47.08-1.09-.42-1.36a4.4,4.4,0,0,0-1.16-.2,3.43,3.43,0,0,1-1.84-1.62,5.37,5.37,0,0,0-1-1.2c-.41-.32-.87-.57-1.25-.93a5.39,5.39,0,0,1-1.16-2.94c-.1-.54-.2-1.08-.35-1.6s-.38-1-.48-1.51C-11.63,3.27-11.09,2.5-10.87,1.76Z" transform="translate(50.82 59)"/>
                    <path class="cls-28" d="M-13.09,2c.42.61-.29,2,1,5.12.28,1.12-.27,1.41.35,2.3.46.68.87,1.67,1.32,2.31a1.78,1.78,0,0,0,.46.51A6.23,6.23,0,0,1-8.77,13.6a11.61,11.61,0,0,1,.92,1.55l.11.17c.7,1,4.07,4.2,6.53,4.38,1.65.45,2.62.29,3.54.91s1.24,0,1.59.49.63.55,1.62.06c.53-.2,4,.55,9-2l0,0h0A44.34,44.34,0,0,0,18.5,17h0c1.27-.85,1.56-1.31,1.72-1.72h0a.83.83,0,0,1,.3-.34h0l.22-.13h0l.24-.13c.21-.14.34-.35.2-.83h0c1.06-1.12,3.88-6.3,3-10a1.56,1.56,0,0,0,.33-1.69c-.15-.22-.43-.31-.86-.17.33-1.2.59-3.35-2.1-7.36a12.53,12.53,0,0,1-.95-2l0,0c-.09-.12-.43,0-.64-.36a.67.67,0,0,1-.11-.24A7.35,7.35,0,0,0,19-9.74,2.75,2.75,0,0,0,17.49-11,2.37,2.37,0,0,1,16-11.81a1,1,0,0,1-.18-.57.68.68,0,0,0-.06-.34c-.11-.16-.36,0-.65.16s-3.33-1.7-6.42-1.59c-.55-.2-1-1.16-1.25-1a.86.86,0,0,0-.21.37,1.38,1.38,0,0,1-.38.68h0A.44.44,0,0,1,6.6-14h0a9.36,9.36,0,0,0-5.91.61c-2,1-2.53,1.38-3.62,1.86h0a5.64,5.64,0,0,0-.85.49h0l-.51.35C-5.26-10-6.39-9-7.39-8.37c-.26.18-.52.34-.78.5s-.52.32-.77.49h0a7,7,0,0,0-2.67,3.22h0v0h0a1.92,1.92,0,0,1-.71.73h0l-.25.17,0,0h0l-.21.13h0a1.59,1.59,0,0,0-.38.34h0a.8.8,0,0,0,0,1,1.59,1.59,0,0,0,.56.48.61.61,0,0,1,.24.21A1.59,1.59,0,0,1-12.52.57C-12.8.76-13.52,1.41-13.09,2Zm1.74-5.29a.6.6,0,0,1,.15-.09l.06,0c.47-.31,1-2,1.77-2.55S-6.42-8.16-5-9.14a31,31,0,0,1,4-2.4C0-12,3.47-13.7,5-13.27c.69.19.8.34.53.52S4.23-12.22,3-11.67a4.46,4.46,0,0,0-.6.32l-.18.14c1,.59,2.73-.47,3.54-1a5,5,0,0,1,2.74-1.11s5,.57,5.7,1.58A3.48,3.48,0,0,0,17-10.38c1.09.13,3.33,3.55,3.54,4.39S23.25-1.17,22.6-.12,23.78,3.4,23,6.53s-1.48,7.27-5.45,9.94l-.19.13c-4.15,2.66-9.83,4-12.46,3.45s-4.64-.57-6.57-1.73-4-1.77-5.62-4.71-3.09-2.84-3.87-6.13-1-5.83-.43-6.23.48-2.54-.18-3S-11.83-2.94-11.35-3.26Z" transform="translate(50.82 59)"/>
                    <path class="cls-29" d="M6.34-11.44a6.23,6.23,0,0,1,7.5,1.69c3.38,3.84,4.22,3.28,3.92,7.08s1.33,7.38-6,12.8S-1.56,16.2-6.38,11.52-10.81,4.77-10.2,1.6c.52-2.7.87-4.9,1.8-6.37s4.7-4.47,7-5.68,1.21,1.22,2.54.69S3.68-10,6.34-11.44Z" transform="translate(50.82 59)"/>
                    <g>
                    <path class="cls-30" d="M18.43-3.45c.31-.73.89,0,1.16.28,0,0,0,.09-.05,0s-.44-.53-.65-.36-.18.45-.22.62a9.64,9.64,0,0,0-.26,2.27,6.1,6.1,0,0,0,.42,2.06l.09.22a3.52,3.52,0,0,0,.45.64c.21.23.3.11.41-.15A5.91,5.91,0,0,0,20.19.32c0-.06.09,0,.1,0a4.47,4.47,0,0,1-.11,2.27.51.51,0,0,1-.79.23,1.39,1.39,0,0,1-.47-.52,5.19,5.19,0,0,1-1-2.79A6.7,6.7,0,0,1,18.43-3.45Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M18,11.3a2,2,0,0,1,2.17-.92s.07.11,0,.1c-.78-.09-1.39.63-1.8,1.2a4.33,4.33,0,0,0-.87,2.23,5,5,0,0,0,.81-.38c.06,0,.15.07.1.11a2.51,2.51,0,0,1-.95.48.13.13,0,0,1-.15-.1A3.24,3.24,0,0,1,18,11.3Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M1.85,4.89A2.26,2.26,0,0,0,3.32,6.48a3.07,3.07,0,0,0,2.07,0A2.42,2.42,0,0,0,6.94,5.36c.29-.67-.15-1.19-.71-1.55a2.67,2.67,0,0,0-1-.39,3.89,3.89,0,0,0-2.31.11C2.33,3.74,1.74,4.2,1.85,4.89Zm4.37-.47C6.56,5,5.69,5.57,5.07,5.8a3.52,3.52,0,0,1-1.22.33,2.75,2.75,0,0,1-.65-.08c-.62-.25-1.38-.83-1.09-1.57C2.32,4,3,3.9,3.53,3.84a4.16,4.16,0,0,1,1.72,0A2.31,2.31,0,0,1,6.22,4.42Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M5.51,18.6c.62.28,1.26,0,1.89,0a5,5,0,0,0,1.8,0c.13,0,.55-.1.61-.24a.61.61,0,0,0-.13-.55l-.08-.1a2.46,2.46,0,0,0-.87-.66,2.83,2.83,0,0,0-2.34-.22,2.49,2.49,0,0,0-1.29.75C4.83,18,5.13,18.42,5.51,18.6Zm.15-.77a3,3,0,0,1,.93-.51,2.51,2.51,0,0,1,1.62-.16l.14,0c.25.21.53.37.78.56A1,1,0,0,1,9.4,18c0,.08,0,.16.06.12-.16.08-.19.26-.37.33a3.31,3.31,0,0,1-1.53.12c-.43,0-.87,0-1.3-.1S5.2,18.24,5.66,17.83Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M-8.62,2.72c.38.1.63.63.84.93.55.81,1.38,2,1.17,3-.11.61-.56,1.17-1.22.84A1.85,1.85,0,0,1-8.6,5.58c0-.14.25,0,.25.14A2.07,2.07,0,0,0-7.9,7.24c.61.63.92-.42.91-.84a4.84,4.84,0,0,0-.81-2c-.16-.28-.34-.55-.52-.82l-.24-.37C-8.72,3-9,3.16-9,3.37s-.1,0-.1-.06C-9.17,3-9,2.63-8.62,2.72Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M-5.32-1.91c.07-.08.19,0,.21.11a.77.77,0,0,1,0,.4c0,.24.32.07.43,0A6,6,0,0,0-3.47-2a3.73,3.73,0,0,0,.72-.71A2.27,2.27,0,0,0-2.58-3a14.08,14.08,0,0,0,.63-1.43,2.51,2.51,0,0,0,.15-.62c-.11-.08-.53.29-.6.35-.32.27-.58.59-.87.88,0,0-.11,0-.09-.07A5.23,5.23,0,0,1-2.29-5.17c.36-.33.84-.26.88.3a4.21,4.21,0,0,1-.61,2,2.8,2.8,0,0,1-.46.63h0A2.9,2.9,0,0,1-3.84-1.18,1.53,1.53,0,0,1-5.26-1C-5.55-1.21-5.51-1.7-5.32-1.91Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M10.16-5.47a.29.29,0,0,1,.17-.23c.4-.23,1,.06,1.36.24s.76.45.75.87-.48.39-.74.39a2.44,2.44,0,0,1-1.09-.27C10.31-4.64,10-5.13,10.16-5.47Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M18,8.15a.93.93,0,0,1,.16-.37l0-.06a.57.57,0,0,1,.21-.13.35.35,0,0,1,.38.22.42.42,0,0,1,0,.46c0,.21-.06.41-.28.44S18,8.4,18,8.15Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M15.13,14.61a.45.45,0,0,1,.52.24.15.15,0,0,1,.07.09A1.75,1.75,0,0,1,15.39,16c-.2.32-.54.62-.93.42C13.62,16,14.46,14.72,15.13,14.61Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M-5.39,9.06A.81.81,0,0,1-5,9.41a.91.91,0,0,1,.29.36.3.3,0,0,1-.1.39.46.46,0,0,1-.5,0,1.22,1.22,0,0,1-.56-.9C-5.9,8.94-5.57,9-5.39,9.06Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M-7-2.09c.11-.12.29,0,.35.15a.79.79,0,0,1,0,.85.48.48,0,0,1-.14.22C-6.86-.81-7-.88-7-.94A.91.91,0,0,1-7-2.09Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M-5,14.27a.11.11,0,0,1,.13,0,.2.2,0,0,1,.15.05h0a.58.58,0,0,1,.23.21.21.21,0,0,1,0,.2.12.12,0,0,1-.12.07.26.26,0,0,1-.27,0,.56.56,0,0,1-.22-.39A.16.16,0,0,1-5,14.27Z" transform="translate(50.82 59)"/>
                    <path class="cls-30" d="M5.58-10.1c.61-.11,2.73-.06,2.63.89,0,.37-.52.51-1,.57A2.28,2.28,0,0,1,5-9.07C4.5-9.63,5-10,5.58-10.1Z" transform="translate(50.82 59)"/>
                    </g>
                    <path class="cls-31" d="M11.16-6.49a2.46,2.46,0,0,0,.81.58,5.69,5.69,0,0,1,.95.65c.54.45.78,1.06.18,1.56,0,0-.16,0-.12-.1C13.69-5,11.52-5.62,11.1-6.47,11.07-6.52,11.14-6.52,11.16-6.49Z" transform="translate(50.82 59)"/>
                    <path class="cls-31" d="M20.14,3.27a22.29,22.29,0,0,1,.57-2.17s.08,0,.07,0a6.36,6.36,0,0,1-.45,2.26C20.27,3.5,20.12,3.36,20.14,3.27Z" transform="translate(50.82 59)"/>
                    <path class="cls-31" d="M17.92,11.13c.5-.59,1.38-1.73,2.28-1.12,0,0,0,.1,0,.08-.89-.33-1.5.75-2.12,1.19C18,11.33,17.85,11.21,17.92,11.13Z" transform="translate(50.82 59)"/>
                    <path class="cls-31" d="M4.91,2.86A2.78,2.78,0,0,1,7.3,4.29c0,.08,0,.09-.09,0A4.78,4.78,0,0,0,4.93,3.11C4.84,3.08,4.75,2.86,4.91,2.86Z" transform="translate(50.82 59)"/>
                    <path class="cls-31" d="M-1.15-5.71s.06,0,.06,0A3.73,3.73,0,0,1-1-4.1c0,.08-.15,0-.15-.08A7.82,7.82,0,0,0-1.15-5.71Z" transform="translate(50.82 59)"/>
                    <path class="cls-31" d="M-8.29,2.56a6.8,6.8,0,0,0,.8,1,8.51,8.51,0,0,1,.64.93c.06.1-.06.12-.12,0-.26-.33-.49-.69-.74-1a3.63,3.63,0,0,1-.63-.93S-8.3,2.54-8.29,2.56Z" transform="translate(50.82 59)"/>
                    <path class="cls-31" d="M8.2,16.35a4.43,4.43,0,0,1,1.94,1.32s0,.07,0,0a8.75,8.75,0,0,0-1.9-1.24C8.15,16.46,8.1,16.33,8.2,16.35Z" transform="translate(50.82 59)"/>
                    <path class="cls-31" d="M-4.84,13.6A3.29,3.29,0,0,1-3.69,14a1.06,1.06,0,0,1,.43,1.14c0,.11-.18,0-.21-.06-.15-.3-.14-.69-.45-.88s-.63-.28-.92-.48C-4.87,13.66-4.89,13.59-4.84,13.6Z" transform="translate(50.82 59)"/>
                    <path class="cls-31" d="M14.78,14.49a1,1,0,0,1,.81-.1,1.1,1.1,0,0,1,.48.58c0,.06-.06.06-.09,0s-.2-.35-.42-.4a1.9,1.9,0,0,0-.67.09C14.8,14.7,14.68,14.54,14.78,14.49Z" transform="translate(50.82 59)"/>
                    <path class="cls-32" d="M13.87-8.13c.76,1,2,1.55,2.56,2.69a4.26,4.26,0,0,1,0,3.34c-.16.41-.68-.11-.74-.32-.29-1-.17-2.08-.52-3.06S14-7.12,13.6-8.07C13.52-8.28,13.78-8.25,13.87-8.13Z" transform="translate(50.82 59)"/>
                    <path class="cls-32" d="M18.16,4.74a2.51,2.51,0,0,1,.32-1.34c.11-.14.36.12.34.24a3.71,3.71,0,0,0,0,1,1.33,1.33,0,0,1,0,.79.2.2,0,0,1-.18.12C18.19,5.55,18.13,5.1,18.16,4.74Z" transform="translate(50.82 59)"/>
                    <path class="cls-32" d="M16.32,10.24c0-.16.23,0,.25.08a1.16,1.16,0,0,1-.07.66,1.63,1.63,0,0,1-.35.68c-.19.16-.47-.12-.47-.31C15.69,10.88,16.33,10.72,16.32,10.24Z" transform="translate(50.82 59)"/>
                    <path class="cls-32" d="M7.41,15.06a2.57,2.57,0,0,1,2,.41,4.31,4.31,0,0,0,1,.42c.32.08,1.11.1,1.17-.33,0-.18.23,0,.26.08.25.85-.93,1.17-1.55,1-1-.29-1.7-1.08-2.68-1.29C7.45,15.32,7.25,15.11,7.41,15.06Z" transform="translate(50.82 59)"/>
                    <path class="cls-32" d="M3.12,16.52a4.45,4.45,0,0,1,1.11-.32,4.46,4.46,0,0,0,1.32-.53c.11-.07.32.12.21.21-.63.57-1.56,1.45-2.48,1.14C3.1,17,2.9,16.65,3.12,16.52Z" transform="translate(50.82 59)"/>
                    <path class="cls-32" d="M-1.13,16.1c.22,0,.45.07.68.09s.33.33.11.35l-.34,0a1,1,0,0,1-.42-.12.28.28,0,0,1-.17-.2A.11.11,0,0,1-1.13,16.1Z" transform="translate(50.82 59)"/>
                    <path class="cls-32" d="M-6.61,12a1.29,1.29,0,0,1,.73.63c0,.05,0,.13,0,.16s-.14,0-.18-.08a1.1,1.1,0,0,0-.5-.38.26.26,0,0,1-.16-.19C-6.76,12-6.7,11.92-6.61,12Z" transform="translate(50.82 59)"/>
                </g>
                <path class="cls-33" d="M-32.14-14.63a4.77,4.77,0,0,1-1.92-3.08,12.74,12.74,0,0,1-.21-1.59c0-.22,0-.43.08-.64a4.07,4.07,0,0,1,.76-1.86,5.41,5.41,0,0,0,1.87,4.07,7.56,7.56,0,0,0,4.33,1.94c.88.18,1.79.28,2.67.4l.38.05,0,0A6.07,6.07,0,0,1-26.39-17a2.52,2.52,0,0,1-.07-.72,11.81,11.81,0,0,0,2.19,1.1,11.56,11.56,0,0,0,2.74.53c1.86.19,3.74.2,5.6.36a31.49,31.49,0,0,1,5.46.85,11.39,11.39,0,0,1,2.39.93A7.85,7.85,0,0,1-6.75-13a2.56,2.56,0,0,1,.2.26,5,5,0,0,1,.59.87l-.07,0a14.92,14.92,0,0,0-6-1.67,8.73,8.73,0,0,1,3,2.31.3.3,0,0,1,.08.17,9.15,9.15,0,0,0-2.28-1.23,10.11,10.11,0,0,0-2.18-.41A13.91,13.91,0,0,0-15.24-13a15.09,15.09,0,0,0-3.22.16,9.44,9.44,0,0,1,1.28.71A7.48,7.48,0,0,1-15-10l.25.38A8.43,8.43,0,0,1-14-7.35,5.4,5.4,0,0,0-15.5-9a8.93,8.93,0,0,0-3.73-2.43,2.92,2.92,0,0,1,.49,1,.14.14,0,0,1,0,.17,9.25,9.25,0,0,0-4.47-1.61c-1.85-.37-3.79-.48-5.61-.72s-3.88-.57-5.32-1.71A6.21,6.21,0,0,1-35-15.6c0-.06,0-.11,0-.17A11.89,11.89,0,0,0-32.14-14.63Z" transform="translate(50.82 59)"/>
                <path class="cls-34" d="M-36.95-13.14a12.94,12.94,0,0,0,3.74,2,15.82,15.82,0,0,0,4.27.69c2.65.12,5.48.05,7.72,1.66.08.05,0,.17,0,.11-2.45-1.56-5.48-1.2-8.25-1.29A11.36,11.36,0,0,1-37-13C-37.11-13.09-37-13.22-36.95-13.14Z" transform="translate(50.82 59)"/>
                <path class="cls-35" d="M-13.47-16.1c2.74-.08,5.41-.66,8.14-.91a10.65,10.65,0,0,1,7.54,1.74s0,.12,0,.07c-2.32-1.5-5-1.59-7.7-1.27s-5.27,1-7.89.55C-13.48-15.93-13.59-16.1-13.47-16.1Z" transform="translate(50.82 59)"/>
                <path class="cls-36" d="M-34-30.23c0-.07.12,0,.11.07-.68,2,0,4.37,1,6.22a9.21,9.21,0,0,0,4.31,4.21c.13.06.19.31,0,.24a8,8,0,0,1-4.74-4.31C-34.31-25.74-35-28.18-34-30.23Z" transform="translate(50.82 59)"/>
                <path class="cls-37" d="M-39.25-28.4c1.76,1.62,3.09,3.61,2.86,6.08,0,.17-.26,0-.27-.09a10,10,0,0,0-.75-3.19,9.92,9.92,0,0,0-1.9-2.73C-39.37-28.39-39.31-28.46-39.25-28.4Z" transform="translate(50.82 59)"/>
                <path class="cls-38" d="M-42.74-34.76a7.24,7.24,0,0,1,3,1.37,4.61,4.61,0,0,1,1.35,2.48c0,.05-.06,0-.07,0a5.12,5.12,0,0,0-1.47-2.29,12.19,12.19,0,0,0-2.78-1.31C-42.81-34.55-42.92-34.81-42.74-34.76Z" transform="translate(50.82 59)"/>
                <path class="cls-39" d="M-49.24-38a7.6,7.6,0,0,0,4,2.21,56.79,56.79,0,0,0,5.84.22,4.8,4.8,0,0,1,3.79,2.95,8,8,0,0,1,.27,5c0,.13-.2,0-.19-.08a10,10,0,0,0-.2-2.56,10.11,10.11,0,0,0-1-2.41,3.77,3.77,0,0,0-1.47-1.62,7.39,7.39,0,0,0-2.5-.65c-1.83-.2-3.67,0-5.45-.57A6.59,6.59,0,0,1-49.35-38C-49.41-38.07-49.31-38.12-49.24-38Z" transform="translate(50.82 59)"/>
                <path class="cls-40" d="M-38.89-28.36a15.34,15.34,0,0,1,2.49,3,10.33,10.33,0,0,1,1,4.3c.11,1.5-.25,3-.09,4.48a5.6,5.6,0,0,0,1.83,3.69c.1.08.07.29-.09.21-2.68-1.36-2.6-4.92-2.63-7.47a22.08,22.08,0,0,0-.56-4.59A9.21,9.21,0,0,0-39-28.28C-39.09-28.38-39-28.44-38.89-28.36Z" transform="translate(50.82 59)"/>
                <path class="cls-41" d="M-5.43,2.72a27.52,27.52,0,0,0,5.31,7A23,23,0,0,0,3,12.21a22.61,22.61,0,0,0,3.81,1.91c.19.08.53.62.12.63a7.69,7.69,0,0,1-4-1.26A19.26,19.26,0,0,1-.68,10.63a20.87,20.87,0,0,1-4.89-7.9C-5.62,2.61-5.47,2.65-5.43,2.72Z" transform="translate(50.82 59)"/>
                <g class="cls-42">
                    <path class="cls-43" d="M-29.9-10.65a27.69,27.69,0,0,1,4.55-.22,19.44,19.44,0,0,1,5,1c.14.05.19-.09.11-.18a7.71,7.71,0,0,0-2.72-2,14.69,14.69,0,0,1,6.26,2.47A9.37,9.37,0,0,1-13.81-5.9c.66,1.5,1.08,3.09,1.74,4.59,0,.11.23.13.23,0a16.68,16.68,0,0,0-1.27-5.44,40.48,40.48,0,0,1,2.4,5c.61,1.35,1.28,2.69,2,4A21.57,21.57,0,0,0-6.2,6a4.78,4.78,0,0,0,3,1.91.09.09,0,0,0,.11-.13A31,31,0,0,0-5.56,2.63,24.2,24.2,0,0,0,.79,9.39C1,9.5,1,9.3.9,9.2-1.65,7.16-2.73,3.78-4,.91q-1.1-2.55-2.33-5c1.35,1.3,2.65,2.63,3.92,4A29.67,29.67,0,0,0,2.56,4.36C3.78,5.17,5.45,6.08,7,5.8c1.69-.31,1.37-2.19.87-3.39A28,28,0,0,0,4.61-2.45c1,.7,1.92,1.59,2.87,2.37A7.17,7.17,0,0,0,10.8,1.77c.09,0,.13-.05.11-.13a5,5,0,0,0-.28-.93c1.17.83,2.23,2,3.71,2.14a.09.09,0,0,0,.11,0,1.69,1.69,0,0,0,.17-.36c.5-1.36-.15-3-.75-4.18a26.93,26.93,0,0,0-2.55-4,28.57,28.57,0,0,0-9-7.45,33.65,33.65,0,0,1,8.72,3.89,21,21,0,0,1,4.07,3.57c1.15,1.3,2.2,2.67,3.33,4,.06.06.22.08.21-.05-.54-3.93-3.32-7.3-6.67-9.47a21.26,21.26,0,0,1,9.89,9.4c.05.09.26.13.23,0a13.29,13.29,0,0,0-6.83-9.71,8.55,8.55,0,0,1,5.76,3.74c.08.13.3.08.21-.08-1.91-3.32-5.34-5-8.72-6.52A23.7,23.7,0,0,1,8-16.9,4.71,4.71,0,0,1,5.87-20c0-.08,0-.16.07-.24a2.84,2.84,0,0,0,.48,1.2,7,7,0,0,0,2.43,2,19.55,19.55,0,0,0,1.8.83,46.67,46.67,0,0,0,6,2.74A19.09,19.09,0,0,1,22.48-9.7a15.61,15.61,0,0,1,3.13,4.47c2.24,4.88,2.31,12.06,0,16.77C23,17,17.76,20.57,11.64,22-3.87,25.67-14,14.81-16.95,3.65c-.51-1.94-.58-3.95-1-5.91,0-.24-.1-.47-.16-.7l-.17-.23a8.57,8.57,0,0,0-.48-1.4A6.62,6.62,0,0,0-20.5-6.94a3.18,3.18,0,0,1,1,.53A5,5,0,0,1-18.31-5.2a7.4,7.4,0,0,0-1.05-1.36c-1.51-1.53-3.9-2-6-2.24s-4.18.07-6.24-.43a12.69,12.69,0,0,1-3.7-1.6A15.34,15.34,0,0,0-29.9-10.65Z" transform="translate(50.82 59)"/>
                </g>
                <g class="cls-44">
                    <path class="cls-45" d="M-16.41-.37c.7,2.39,1.28,5,2.92,6.91,0,0,.11,0,.1,0-.31-1.75-1.11-3.41-1.1-5.2,1.31,2.73,1.82,6,4.21,8.09a.05.05,0,0,0,.09,0c-.24-1.89-1.5-3.58-1.33-5.52,1.4,3.19,2,7.37,5.35,9.2,0,0,.08,0,.07-.05-.22-.88-.91-1.63-1-2.54A20.38,20.38,0,0,1-5,12.42c.83.82,1.66,1.65,2.55,2.4C-1,16,1,17.42,2.87,16.62a0,0,0,0,0,0-.06c-.79-2-2.78-3.33-3.36-5.41,1.16.62,2,1.7,3.31,2.12.05,0,.06,0,.05-.08-.41-.86-1.09-1.56-1.53-2.41C4.13,12.45,6.66,15,10,15.34c0,0,.05,0,.05,0,.1-1-.7-2-1.29-2.74A15.76,15.76,0,0,1,6.65,9.68c1.91,1,3.12,3.24,5.38,3.59a.06.06,0,0,0,.06-.07,9.27,9.27,0,0,1-.7-1.78,24.5,24.5,0,0,1,2.92,1.52,3.93,3.93,0,0,0,2.85.52,0,0,0,0,0,0-.06c-.85-3.17-3.87-5.21-4.91-8.3,1.27,1.69,2.48,3.67,4.45,4.61,0,0,.08,0,.06-.05A21.33,21.33,0,0,1,15.72,6.5,20.22,20.22,0,0,0,19.2,10s.08,0,.08,0c0-2.66-1.31-5.16-2.39-7.55a19.93,19.93,0,0,1,3.88,7c0,.06.13.08.11,0-.09-.4,0-.81-.07-1.21A10.31,10.31,0,0,1,22.37,11s.1.06.11,0a12.84,12.84,0,0,0,.12-6.16A26,26,0,0,0,20.46-.83,21.63,21.63,0,0,1,23.58,2.6s.11,0,.1,0a7.44,7.44,0,0,0-.52-1.9,7.84,7.84,0,0,1,1.71,2.5s.1.06.11,0C25.47,0,24.79-4,22.13-6.13A9.54,9.54,0,0,1,24.4-4.19s.08,0,.08,0c-.18-2.37-1.87-4.36-3.55-5.93a44.61,44.61,0,0,0-4.31-3.42l.05,0A19.09,19.09,0,0,1,22.48-9.7a15.61,15.61,0,0,1,3.13,4.47c2.24,4.88,2.31,12.06,0,16.77C23,17,17.76,20.57,11.64,22-3.87,25.67-14,14.81-16.95,3.65c-.51-1.94-.58-3.95-1-5.91,0-.24-.1-.47-.16-.7l-.17-.23a8.57,8.57,0,0,0-.48-1.4,6.27,6.27,0,0,0-1.11-1.72C-17.92-4.92-17.07-2.59-16.41-.37Z" transform="translate(50.82 59)"/>
                </g>
                <g>
                    <path class="cls-46" d="M-23.61-29.27c.24.79,1.33.84,2,1.13a4.66,4.66,0,0,1,.5.25A4.4,4.4,0,0,1-19.8-26.8a4.75,4.75,0,0,1,.74,1.29c.14.53.38,1.6,0,1.84a2.41,2.41,0,0,0-.24-.73,2.76,2.76,0,0,0-1.88-1.31c-1-.24-2.24-.49-2.56-1.67A3.48,3.48,0,0,1-23.61-29.27Z" transform="translate(50.82 59)"/>
                    <path class="cls-47" d="M-23.49-28.93c.2.17.37.36.58.52a2.85,2.85,0,0,0,.89.35,4.29,4.29,0,0,1,1.29.63,4,4,0,0,1,1.59,2.37c0,.18-.19.07-.23,0a6.06,6.06,0,0,0-1.95-2.28c-.42-.28-.91-.43-1.35-.68a2.78,2.78,0,0,1-.88-.86C-23.59-28.95-23.53-29-23.49-28.93Z" transform="translate(50.82 59)"/>
                </g>
                <g>
                    <path class="cls-48" d="M-43.36-22.29l-.27-.26a3.12,3.12,0,0,1-.88-1.88,3.38,3.38,0,0,1,2,2.86,3.33,3.33,0,0,1,0,.55C-42.47-21.42-43-21.92-43.36-22.29Z" transform="translate(50.82 59)"/>
                    <path class="cls-49" d="M-44.16-24a3.83,3.83,0,0,1,1.36,1.6c.06.16-.14.16-.2.05a7.74,7.74,0,0,0-1.19-1.6S-44.2-24-44.16-24Z" transform="translate(50.82 59)"/>
                </g>
                <g>
                    <path class="cls-50" d="M-42.18-13.2l-.27-.26a3.14,3.14,0,0,1-.88-1.88,3.36,3.36,0,0,1,2,2.86,3.33,3.33,0,0,1,0,.55C-41.29-12.33-41.81-12.83-42.18-13.2Z" transform="translate(50.82 59)"/>
                    <path class="cls-51" d="M-43-15.08c.22.33.6.53.85.84a2.86,2.86,0,0,1,.52,1c0,.14-.17.05-.2,0a7.12,7.12,0,0,0-.68-1.12c-.2-.24-.47-.41-.58-.71C-43.11-15.13-43-15.12-43-15.08Z" transform="translate(50.82 59)"/>
                </g>
                <g>
                    <path class="cls-52" d="M-24.41-3.52a13,13,0,0,0-.49-1.41,2,2,0,0,0-.25-.39l.14.06a5.74,5.74,0,0,1,1.76,1.41,3.52,3.52,0,0,1,.69,2.18A4.09,4.09,0,0,1-24.41-3.52Z" transform="translate(50.82 59)"/>
                    <path class="cls-53" d="M-24.63-4.83c.62.66,1.67,1.33,1.76,2.3,0,.2-.22.1-.27,0a5.19,5.19,0,0,0-.77-1.28c-.26-.32-.55-.62-.78-1C-24.73-4.84-24.67-4.87-24.63-4.83Z" transform="translate(50.82 59)"/>
                </g>
                <path class="cls-54" d="M19.3-10.9c.64.49,1.36.88,2,1.43a19.44,19.44,0,0,1,1.9,2,15.11,15.11,0,0,1,2.49,4.58,17.35,17.35,0,0,1-.1,11c-.05.15-.31-.05-.29-.16.43-3.54.83-7.15-.18-10.64a15.89,15.89,0,0,0-2-4.45,12.73,12.73,0,0,0-1.69-2c-.65-.63-1.39-1.1-2.09-1.66C19.2-10.85,19.22-11,19.3-10.9Z" transform="translate(50.82 59)"/>
                <path class="cls-55" d="M16.7-8.61A11.58,11.58,0,0,1,21.26-4.5,17.49,17.49,0,0,1,24.14,2a16.9,16.9,0,0,1,0,7.22,15.58,15.58,0,0,1-3,6.3c-.09.12-.32-.1-.26-.21A21.63,21.63,0,0,0,22.94,2.16a17.32,17.32,0,0,0-2.09-6.07,11.64,11.64,0,0,0-4.18-4.54C16.58-8.5,16.58-8.67,16.7-8.61Z" transform="translate(50.82 59)"/>
                <path class="cls-56" d="M5.27,18c4.17.4,8.59-.18,11.87-3a12.9,12.9,0,0,0,3.75-5.63,22.77,22.77,0,0,0,.93-3.54,9.23,9.23,0,0,0,0-3.95c0-.1.12,0,.13,0,.72,2.43.18,5-.49,7.36A13,13,0,0,1,18,15.21c-3.06,3-7.51,3.8-11.67,3.21q-.53-.08-1.05-.18C5.2,18.22,5.11,18,5.27,18Z" transform="translate(50.82 59)"/>
                <path class="cls-57" d="M-9.37,13.93a48.16,48.16,0,0,0,5.85,3.82A18.65,18.65,0,0,0,3.7,19.69a28.88,28.88,0,0,0,7.71-.42,28,28,0,0,0,3.72-1,12.46,12.46,0,0,0,3.36-1.53c.06,0,.18.07.12.12a11.54,11.54,0,0,1-3,1.64A26.59,26.59,0,0,1,12,19.73a27,27,0,0,1-7.76.7A18.91,18.91,0,0,1-3,18.71a29.7,29.7,0,0,1-6-4.1l-.5-.41C-9.75,14.05-9.6,13.77-9.37,13.93Z" transform="translate(50.82 59)"/>
                <path class="cls-58" d="M-17.47-5.34A34.66,34.66,0,0,1-16-1.95a29.59,29.59,0,0,1,.78,4.18,64.06,64.06,0,0,0,2,7.83c0,.13-.16.06-.19,0A33,33,0,0,1-15,6.39a25.78,25.78,0,0,1-.91-4c-.23-1.33-.29-2.68-.5-4a23.2,23.2,0,0,0-1.21-3.73C-17.62-5.43-17.5-5.41-17.47-5.34Z" transform="translate(50.82 59)"/>
                <path class="cls-59" d="M-11.38.6a22.62,22.62,0,0,0,3.24,10c1.78,2.94,4.28,5.64,7.75,6.52.21.06.41.1.62.14s.25.28.05.26C-3.2,17.17-6.06,14.86-8,12.1a21.44,21.44,0,0,1-3.46-9.77,15.39,15.39,0,0,1-.11-1.8C-11.55.42-11.38.53-11.38.6Z" transform="translate(50.82 59)"/>
                <path class="cls-60" d="M16.29-13.18c.81.52,1.65,1,2.46,1.5A12.67,12.67,0,0,1,21-9.82a20,20,0,0,1,3.38,4.91,15,15,0,0,1,1.4,10.7c0,.11-.24,0-.22-.12.73-3.49-.2-7.24-1.69-10.42a20.69,20.69,0,0,0-3.12-4.83,25.09,25.09,0,0,0-4.45-3.54S16.23-13.22,16.29-13.18Z" transform="translate(50.82 59)"/>
                <path class="cls-61" d="M22.52,13.31a18.34,18.34,0,0,0,1.63-13c0-.08.09,0,.1,0,1.22,4.28,1,9.34-1.58,13.11C22.62,13.51,22.48,13.38,22.52,13.31Z" transform="translate(50.82 59)"/>
                <path class="cls-62" d="M-.41,20.29a25.13,25.13,0,0,0,5.35,1.14,19.69,19.69,0,0,0,5.47-.34,24.42,24.42,0,0,0,4.8-1.54,17.39,17.39,0,0,0,4.32-2.64s.09,0,.06.06a19.19,19.19,0,0,1-8.86,4.47A19.51,19.51,0,0,1-.43,20.58C-.59,20.51-.64,20.22-.41,20.29Z" transform="translate(50.82 59)"/>
                <path class="cls-63" d="M-18.47-10.54a7.61,7.61,0,0,0,1,.69,5.3,5.3,0,0,1,.8.84,11.25,11.25,0,0,1,1.44,2.24,10.11,10.11,0,0,1,1,5.43c0,.12-.18,0-.18-.07a13.82,13.82,0,0,0-1.21-5.33,10.75,10.75,0,0,0-1.26-2.18,4.34,4.34,0,0,0-1-1,3.36,3.36,0,0,1-.66-.59S-18.49-10.56-18.47-10.54Z" transform="translate(50.82 59)"/>
                <path class="cls-64" d="M-15.84-11.41A12.09,12.09,0,0,1-11-7.18,23.63,23.63,0,0,1-8.7-.58C-7.65,3.52-6.82,8-4,11.32c.24.28.5.56.76.82s0,.23-.13.13a17.47,17.47,0,0,1-4.1-5.18,19.59,19.59,0,0,1-1-2.34,26.17,26.17,0,0,0,.73,2.73A19.68,19.68,0,0,0-4.9,12.8,10.81,10.81,0,0,0,.46,16.61c.08,0,.14.2,0,.17a13.09,13.09,0,0,1-5.64-3.2A13.15,13.15,0,0,1-8.62,8.17a34.22,34.22,0,0,1-1.59-6.92c-.27-1.8-.43-3.63-.77-5.43-.13-.47-.26-.93-.41-1.39a9.6,9.6,0,0,0-1.5-3,11.86,11.86,0,0,0-2.43-2.26l-.56-.4C-15.95-11.3-16-11.48-15.84-11.41Z" transform="translate(50.82 59)"/>
                <path class="cls-65" d="M-9.54-15A11.67,11.67,0,0,0-6-14.72a35.59,35.59,0,0,1,3.58.14,32.91,32.91,0,0,1,7.63,1.47,20,20,0,0,1,6.55,3.49,16.51,16.51,0,0,1,3.84,5c.33.61.64,1.24.94,1.87.08.18-.17.18-.24,0A22.2,22.2,0,0,0,12.47-8a20.5,20.5,0,0,0-6.14-3.6,36.6,36.6,0,0,0-7.5-1.94C-2.44-13.71-3.73-13.89-5-14a25,25,0,0,1-3.56-.47,6.36,6.36,0,0,1-1-.32C-9.6-14.85-9.66-15-9.54-15Z" transform="translate(50.82 59)"/>
                <path class="cls-66" d="M9.3-12.7C11-11.52,13-10.89,14.67-9.58a17.62,17.62,0,0,1,4.17,4.65A15.76,15.76,0,0,1,21,.75a17.08,17.08,0,0,1,.21,3,10.69,10.69,0,0,1-.41,3.08c-.08.25-.43-.09-.44-.24,0-1,0-2,0-2.93A19.23,19.23,0,0,0,20,.68a17.72,17.72,0,0,0-2.05-5.39,21.09,21.09,0,0,0-3.65-4.56c-1.45-1.4-3.44-2.1-5-3.33C9.19-12.65,9.21-12.76,9.3-12.7Z" transform="translate(50.82 59)"/>
                <path class="cls-67" d="M-11.23-13.46a47.2,47.2,0,0,0,6.07,2A32.41,32.41,0,0,1,0-9.31C3.16-7.6,6.31-5,7.49-1.56c.13.38-.35.29-.47,0A16.21,16.21,0,0,0-.49-8.87,39.81,39.81,0,0,0-5.41-11c-2-.72-4.1-1.11-5.85-2.33C-11.31-13.39-11.32-13.51-11.23-13.46Z" transform="translate(50.82 59)"/>
                <path class="cls-68" d="M-11.44-10.56c3.84,2.14,6,6.2,7,10.29.1.38-.44.18-.53,0-.79-1.85-1.46-3.76-2.38-5.55a11.46,11.46,0,0,0-4.13-4.63C-11.54-10.51-11.52-10.61-11.44-10.56Z" transform="translate(50.82 59)"/>
                </g>
                <path class="cls-69" d="M13.54-16.78a38.48,38.48,0,0,1,9.23,4.88,17.21,17.21,0,0,1,3.46,3.62A11.4,11.4,0,0,1,28.3-3.41c0,.25-.28.12-.35,0A26.49,26.49,0,0,0,25.62-8a17.68,17.68,0,0,0-3.27-3.48,35.3,35.3,0,0,0-8.83-5C13.37-16.58,13.32-16.87,13.54-16.78Z" transform="translate(50.82 59)"/>
                <path class="cls-70" d="M-16.08,7.88a23,23,0,0,0,5.61,8.58,29.39,29.39,0,0,0,8.52,5.89c.13.06.18.32,0,.24A26.53,26.53,0,0,1-11,17a17.87,17.87,0,0,1-5.21-9.13C-16.24,7.78-16.1,7.82-16.08,7.88Z" transform="translate(50.82 59)"/>
                <path class="cls-71" d="M-12.29,16.7a16.23,16.23,0,0,0,2.11,1.86c.22.16.15.5-.14.33a7.41,7.41,0,0,1-2.11-2.1C-12.52,16.67-12.38,16.61-12.29,16.7Z" transform="translate(50.82 59)"/>
                <path class="cls-72" d="M26.85,12.11a24.72,24.72,0,0,0,2.42-8.67,23.14,23.14,0,0,0,.1-4.78,37.07,37.07,0,0,0-.84-4.78c0-.13.16,0,.18,0a40.39,40.39,0,0,1,1.12,4.84,19.92,19.92,0,0,1,0,4.94c-.28,3-.83,6.18-2.69,8.68C27.06,12.51,26.78,12.25,26.85,12.11Z" transform="translate(50.82 59)"/>
                <path class="cls-73" d="M28.3-1.4c0-.17.22-.07.24,0A6.57,6.57,0,0,1,28.69.77c0,.21-.32,0-.34-.11C28.25,0,28.36-.72,28.3-1.4Z" transform="translate(50.82 59)"/>
            </g>

            <path id="trajetbas" class="cls-174" d="M251.5,429.5q-1-122-2-244" transform="translate(50.82 59)"/>
            <path id="trajethaut" class="cls-175" d="M-.5.5c60,31,118.35,67.24,176,106,26.75,18,52,36,74,59a715.42,715.42,0,0,1,250-147" transform="translate(50.82 59)"/>
            
            <g id="boom">
                <g id="etincelles">
                <g>
                    <path class="cls-76" d="M194,67.33S211.88,86,224.91,78.26c14.61-8.64,22.29-34.55,22.29-34.55s-2.56,35,22.37,37.53c19.62,2,40-34.71,40-34.71S288,85.16,308.07,108.58c20.68,24.11,62,37.37,139.38,46.54,0,0-113.64,20.6-62.86,71.72,0,0-59.82-36.11-97-17.43s-55.3,45-79.49,106c0,0,27-73.92,6.56-103.19s-70.53,8.65-87.91,24.52c0,0,46.68-42.5,45.66-74.58s-49.74-38.74-49.74-38.74,39,9.12,68.82-11C215.61,96.24,194,67.33,194,67.33Z" transform="translate(50.82 59)"/>
                    <path class="cls-77" d="M194,67.33s12.47,19.42,27.6,12C238.58,71,247.2,43.71,247.2,43.71s-7.84,34.52,17,37.53,45.32-34.71,45.32-34.71-26,39-6.82,62.05c20.34,24.43,62,37.37,139.38,46.54,0,0-111.77,18.68-61,69.8,0,0-61.68-34.19-98.82-15.51s-50.43,46.19-74.63,107.18c0,0,22.15-75.09,1.7-104.36s-65.77,9.17-83.15,25c0,0,41.87-43,40.9-75.1-.94-31-66.79-42.66-66.79-42.66s57.26,11.15,85.87-7.05C214.44,94.49,194,67.33,194,67.33Z" transform="translate(50.82 59)"/>
                    <path class="cls-78" d="M206.83,83.69s10.49,16.83,22.46,11.14,15.06-28.51,15.06-28.51-2.17,25.31,14.86,29.86c13.05,3.48,35.24-23.73,35.24-23.73s-22.55,25.84-8.18,42.93,61,32.6,115.3,39c0,0-82.32,16.44-46.65,52.35,0,0-56.92-33.68-83-20.55s-32.77,41.29-49.77,84.14c0,0,12.91-61.6-1.46-82.16s-55.19,12.28-67.4,23.42c0,0,38.44-36.05,37.72-58.59s-51.54-26.44-51.54-26.44,43.57,4.93,64.94-8.48C222.62,106.67,206.83,83.69,206.83,83.69Z" transform="translate(50.82 59)"/>
                    <path class="cls-79" d="M212,91.79s12.21,13.88,22.64,8.92,8.9-25.9,8.9-25.9,2.32,23.12,17.17,27.08c11.37,3,26.94-20.71,26.94-20.71s-15.89,22.55-3.37,37.44,55.4,30.71,102.74,36.32c0,0-74,12-42.93,43.3,0,0-49.59-29.34-72.33-17.91s-31,37.48-45.79,74.81c0,0,13.67-55.17,1.16-73.08s-48.09,10.7-58.73,20.41c0,0,33.49-31.41,32.86-51.05s-44.9-23-44.9-23,38,4.3,56.59-7.39C228.79,111,212,91.79,212,91.79Z" transform="translate(50.82 59)"/>
                    <polygon class="cls-80" points="349.13 194.58 467.61 133.93 470.31 136.53 349.13 194.58"/>
                    <polygon class="cls-80" points="355.56 184.74 406.4 133.99 410.27 135.78 355.56 184.74"/>
                    <polygon class="cls-80" points="351.69 162.91 351.69 130.49 356.38 130.49 351.69 162.91"/>
                    <polygon class="cls-80" points="355.39 169.43 380.81 134.18 385.02 135.58 355.39 169.43"/>
                    <polygon class="cls-80" points="188.46 117.27 192.65 115.85 233.73 171.91 188.46 117.27"/>
                    <polygon class="cls-80" points="188.26 151.74 191.79 149.65 258.77 201.9 188.26 151.74"/>
                    <polygon class="cls-80" points="215.47 191.45 135.96 131.51 139.43 129.38 215.47 191.45"/>
                    <polygon class="cls-80" points="236.1 208.48 144.57 173.88 146.84 171.1 236.1 208.48"/>
                    <polygon class="cls-80" points="192.67 199.15 107.98 181.7 109.34 178.66 192.67 199.15"/>
                    <polygon class="cls-80" points="148.82 271.69 146.98 268.76 235.98 246.31 148.82 271.69"/>
                    <polygon class="cls-80" points="149.61 324.88 146.19 322.7 272.77 231.07 149.61 324.88"/>
                    <polygon class="cls-80" points="192.74 271.56 190.23 268.88 233.8 250.08 192.74 271.56"/>
                    <polygon class="cls-80" points="328.02 323.79 323.33 323.79 323.33 260.82 328.02 323.79"/>
                    <polygon class="cls-80" points="367.97 366.58 335.9 298.91 372.43 365.6 367.97 366.58"/>
                    <polygon class="cls-80" points="367.77 332.84 335.95 278.56 372.12 331.66 367.77 332.84"/>
                    <polygon class="cls-80" points="383.76 237.87 483.93 208.12 485.62 211.08 383.76 237.87"/>
                    <polygon class="cls-80" points="408.62 237.97 458.89 230.58 459.46 233.74 408.62 237.97"/>
                    <polygon class="cls-81" points="343.46 191.24 461.93 130.59 464.63 133.19 343.46 191.24"/>
                    <polygon class="cls-81" points="349.88 181.4 400.72 130.65 404.6 132.44 349.88 181.4"/>
                    <polygon class="cls-81" points="346.02 159.57 346.02 127.14 350.7 127.14 346.02 159.57"/>
                    <polygon class="cls-81" points="349.71 166.08 375.13 130.84 379.34 132.24 349.71 166.08"/>
                    <polygon class="cls-81" points="182.78 113.93 186.98 112.51 228.05 168.57 182.78 113.93"/>
                    <polygon class="cls-81" points="182.58 148.4 186.12 146.31 253.1 198.56 182.58 148.4"/>
                    <polygon class="cls-81" points="209.8 188.11 130.28 128.17 133.76 126.04 209.8 188.11"/>
                    <polygon class="cls-81" points="230.43 205.14 138.89 170.54 141.17 167.76 230.43 205.14"/>
                    <polygon class="cls-81" points="186.99 195.81 102.31 178.36 103.67 175.32 186.99 195.81"/>
                    <polygon class="cls-81" points="143.15 268.34 141.3 265.42 230.3 242.97 143.15 268.34"/>
                    <polygon class="cls-81" points="143.93 321.54 140.52 319.36 267.1 227.73 143.93 321.54"/>
                    <polygon class="cls-81" points="187.06 268.22 184.55 265.54 228.12 246.74 187.06 268.22"/>
                    <polygon class="cls-81" points="322.34 320.45 317.65 320.45 317.65 257.48 322.34 320.45"/>
                    <polygon class="cls-81" points="362.3 363.24 330.22 295.57 366.76 362.26 362.3 363.24"/>
                    <polygon class="cls-81" points="362.09 329.5 330.27 275.22 366.44 328.32 362.09 329.5"/>
                    <polygon class="cls-81" points="378.08 234.53 478.25 204.78 479.95 207.74 378.08 234.53"/>
                    <polygon class="cls-81" points="402.94 234.63 453.22 227.24 453.79 230.4 402.94 234.63"/>
                </g>
                <g>
                    <path d="M393.56,186.24a10,10,0,0,1-3.54.32h0a6.93,6.93,0,0,1-6.21-5.62c-.92-3.66-.09-7.71,2.47-12.05a21.73,21.73,0,0,1,7.57-7.72,13.47,13.47,0,0,1,3.41-1.48,9.94,9.94,0,0,1,4-.27,6.91,6.91,0,0,1,6.27,5.57c.92,3.7,0,7.77-2.71,12.1a22.18,22.18,0,0,1-7.46,7.52A15.08,15.08,0,0,1,393.56,186.24Zm-5.84-89.13L413.29,91l-.91,4.14c-2.55,11.58-4.34,20.65-5.31,27a229.72,229.72,0,0,0-2.41,27.26l-.06,1.81-17.77,6.71Z" transform="translate(50.82 59)"/>
                    <path class="cls-82" d="M390.08,181.65a8.7,8.7,0,0,1-3.09.29,5.65,5.65,0,0,1-5.12-4.62c-.82-3.3,0-7,2.32-11A20.47,20.47,0,0,1,391.3,159a12.07,12.07,0,0,1,3.1-1.35,8.66,8.66,0,0,1,3.49-.23,5.6,5.6,0,0,1,5.18,4.58c.82,3.31,0,7-2.55,11a20.71,20.71,0,0,1-7,7.09A13.42,13.42,0,0,1,390.08,181.65Zm-3.45-29.58a1.28,1.28,0,0,1-1.07-.2,1.36,1.36,0,0,1-.54-1.11l.81-54.83a1.31,1.31,0,0,1,1-1.27L406.42,90a1.27,1.27,0,0,1,1.2.35,1.42,1.42,0,0,1,.35,1.25c-2.56,11.6-4.35,20.7-5.33,27a232.57,232.57,0,0,0-2.43,27.43,1.33,1.33,0,0,1-.84,1.21L386.75,152Z" transform="translate(50.82 59)"/>
                    <path class="cls-83" d="M393.91,161.4a8.65,8.65,0,0,1,5.8-1.42,4.31,4.31,0,0,1,4.07,3.59q1.11,4.43-2.37,10a19.51,19.51,0,0,1-6.58,6.66,9.55,9.55,0,0,1-5.76,1.61,4.33,4.33,0,0,1-4-3.63q-1.11-4.43,2.17-10A19,19,0,0,1,393.91,161.4Zm-4.83-64.25,19.59-4.66q-3.85,17.54-5.34,27.13a233.37,233.37,0,0,0-2.44,27.59L388.27,152Z" transform="translate(50.82 59)"/>
                    <path d="M387.12,96l19.59-4.66q-3.87,17.53-5.34,27.13A231.27,231.27,0,0,0,398.93,146l-12.62,4.77L387.12,96m7.61,63a7.36,7.36,0,0,1,3-.2,4.34,4.34,0,0,1,4.08,3.59q1.1,4.42-2.38,10a19.34,19.34,0,0,1-6.58,6.66,12.12,12.12,0,0,1-3.1,1.36,7.39,7.39,0,0,1-2.65.25,4.35,4.35,0,0,1-4-3.62q-1.1-4.44,2.18-10a19,19,0,0,1,6.64-6.79,10.68,10.68,0,0,1,2.79-1.22m-8.26-65.61a2.64,2.64,0,0,0-1.93,2.54l-.81,54.83a2.68,2.68,0,0,0,1.09,2.21,2.47,2.47,0,0,0,2.13.41l.25-.07,12.61-4.77a2.65,2.65,0,0,0,1.69-2.42,230,230,0,0,1,2.42-27.27c1-6.3,2.75-15.37,5.3-26.95a2.72,2.72,0,0,0-.69-2.48,2.51,2.51,0,0,0-2.4-.71l-19.59,4.66-.07,0Zm7.61,63a13.62,13.62,0,0,0-3.43,1.5,21.78,21.78,0,0,0-7.55,7.7h0c-2.55,4.33-3.38,8.39-2.46,12.05a6.91,6.91,0,0,0,6.2,5.61h0a10.05,10.05,0,0,0,3.54-.32,15.3,15.3,0,0,0,3.75-1.64,22.07,22.07,0,0,0,7.45-7.52c2.72-4.33,3.63-8.4,2.71-12.09a6.91,6.91,0,0,0-6.28-5.57,10.07,10.07,0,0,0-4,.26Z" transform="translate(50.82 59)"/>
                </g>
                </g>
                    <g id="texte">
                    <path class="cls-84" d="M172.57,119.85a17.83,17.83,0,0,0-2.71,4.69,53.17,53.17,0,0,0-1.84,7.54l-1.8,9.4.38,1.12,1.93,1.46-6.66.53,1.78-1.76-3.32-9.15L158.54,143l2.75,2.81-7.46.59-.32-.37,7-.55-2.39-2.35L160,133l-.62-1.7-2.12,11.18.3.88,1.84,1.47-6.67.53,1.86-1.76-5.47-15-2.34-1.67,7.14-.56-1.85,2,4.3,11.76.37-1.81-3.61-9.82,1.23-1.29.35.21-1.12,1.18,3.28,9L159.12,126l1.38-.11,4.89,13.45.33-1.69-4.32-11.84.41,0,4,11.13,1-5.05a30.14,30.14,0,0,0,.81-6.14,2.73,2.73,0,0,0-1.66-2.34,27.86,27.86,0,0,0,3.55-1.77,16.1,16.1,0,0,0,2.79-1.91Zm1,1a17.31,17.31,0,0,0-2.53,4.58,43.45,43.45,0,0,0-1.78,7l-1.84,9.64,3.07,3-7.49.6-.37-.37,6.94-.55-2.59-2.54,1.91-10a45.75,45.75,0,0,1,1.76-6.88,19.55,19.55,0,0,1,2.57-4.7Z" transform="translate(50.82 59)"/>
                    <path class="cls-85" d="M187.76,123.68l-.21.3a14.24,14.24,0,0,0-2,4.37l-.26-.47a4.79,4.79,0,0,0-1.91-2.13,7.41,7.41,0,0,0-3.34-.25c-.73.06-1.6.15-2.62.28l.53,6.72.94-.07-.47-6c.73-.12,1.43-.22,2.11-.27a7,7,0,0,1,2.76.18,3.68,3.68,0,0,1,1.68,1.41l-.31-.2a6.94,6.94,0,0,0-1.79-1,8,8,0,0,0-2.09,0c-.31,0-.87.09-1.66.19l-.31,0,.44,5.55,2.69-.22,1.68-2.73.5,6.29L182,133.45l-4,.32.65,8.19.93-.07-.59-7.51,3.16-.25.41.35-3.18.25.57,7.13a14.93,14.93,0,0,0,4.2-.76,3.62,3.62,0,0,0,1.84-1.82l.66-1.39.15-.34a9.61,9.61,0,0,0,1.15,2.5,12.11,12.11,0,0,0,2,2.1l-16.37,1.3,2.09-1.87-.52-6.51a5.38,5.38,0,0,0-2.16.93l-.23-.29a5.21,5.21,0,0,1,2.36-1l-.06-.69h-.35a4,4,0,0,0-2.63,1.31,3,3,0,0,1,2.88-2.58l-.49-6.24-2.36-1.57Zm3.93,18.42-.65,1.38-15.95,1.26-.26-.37,16-1.27.42-1a11.25,11.25,0,0,1-3.29-3.91l.48.14a12.07,12.07,0,0,0,3,3.52Zm-7.05-12.25.51,6.45-.38-.16-.51-6.45Zm3.9-5.2-.09.14a17.21,17.21,0,0,0-2.06,4.2l-.27-.29a17.18,17.18,0,0,1,2.19-4.34Z" transform="translate(50.82 59)"/>
                    <path class="cls-86" d="M190.52,123.46l7-.55-1.9,2.16,1.24,15.61h.78l-1.24-15.51,1.46-1.71.23.3-1.31,1.56L198,140.63c.44,0,.86,0,1.26,0a5.23,5.23,0,0,0,3.43-1.31,5.57,5.57,0,0,0,1.33-3.52,15,15,0,0,0,3.25,5.22L192,142.2l1.88-1.81-1.21-15.24Zm18.16,17.29-.56,1.37-15.24,1.21-.4-.36,15.45-1.23.35-1a16.56,16.56,0,0,1-2.92-4.19l-.07-.15.48.14.12.24a21.89,21.89,0,0,0,2.66,3.8Z" transform="translate(50.82 59)"/>
                    <path class="cls-87" d="M222.27,121.39l-.14.29a42.73,42.73,0,0,0-1.91,4.77l-.1.35-.2-.09a6.44,6.44,0,0,0-1.84-3.4,4,4,0,0,0-3.07-1.06,3.77,3.77,0,0,0-3,1.87q-1.39,2.13-1,6.95a12.42,12.42,0,0,0,2,6.39,4.46,4.46,0,0,0,4.18,2,4.18,4.18,0,0,0,3.15-1.62,6.66,6.66,0,0,0,1.14-4c0-.42.16-.63.45-.66s.6.27.65.91a7.56,7.56,0,0,1-.41,3c-.74,2.22-2.52,3.43-5.35,3.66a7.5,7.5,0,0,1-6.25-2.19,10.87,10.87,0,0,1-2.8-7,10.69,10.69,0,0,1,1.76-7.21A7.2,7.2,0,0,1,215,121a10.58,10.58,0,0,1,3.79.5,5.12,5.12,0,0,0,1.85.28,2.43,2.43,0,0,0,1.35-.53Zm-4,2.87a4.08,4.08,0,0,0-2.6-.87q-3.78.3-3.21,7.51a15,15,0,0,0,1.31,5.4,4.69,4.69,0,0,0,2.7,2.63v.15a4.1,4.1,0,0,1-3-2.43,14.58,14.58,0,0,1-1.49-5.74q-.6-7.55,3.55-7.88a3.53,3.53,0,0,1,2.68,1Zm5.56,9.65a8.62,8.62,0,0,1-1.59,5.67,6.55,6.55,0,0,1-4.85,2.29,7.67,7.67,0,0,1-4.66-1.14v-.12a8.06,8.06,0,0,0,4.61.87,6.35,6.35,0,0,0,4.57-2.23,5.65,5.65,0,0,0,1.15-2.14,10.87,10.87,0,0,0,.33-3v-.34Zm-.41-11.9-.13.29a42.5,42.5,0,0,0-2.15,5.15l-.41-.16.13-.38q.61-1.59,1.08-2.64l1-2.16.12-.28Z" transform="translate(50.82 59)"/>
                    <path class="cls-88" d="M232.47,119.94a6.88,6.88,0,0,1,5.89,2.23,11.25,11.25,0,0,1,2.69,7,10.31,10.31,0,0,1-1.55,6.94,7,7,0,0,1-5.43,3,7.66,7.66,0,0,1-5.11-1.38q-3.41-2.39-3.81-7.53a10.56,10.56,0,0,1,1.63-7.19A7.31,7.31,0,0,1,232.47,119.94Zm.1,1.25q-4.94.39-4.24,9.07.63,8,5.49,7.63a3.75,3.75,0,0,0,3.28-2.4,13,13,0,0,0,.69-6.31c-.24-3.08-.79-5.21-1.65-6.36A3.83,3.83,0,0,0,232.57,121.19Zm3.3,1.94-.26-.12a5.43,5.43,0,0,0-2.63-.57q-3.84.3-3.24,7.85.44,5.52,3,7.13l-.12.16a5.76,5.76,0,0,1-2.3-2.56,13.67,13.67,0,0,1-1-4.52q-.65-8.1,3.62-8.44A3.24,3.24,0,0,1,235.87,123.13Zm1.32-2.82a7.21,7.21,0,0,1,3.57,3.18,12.56,12.56,0,0,1,1.66,5.54,11.58,11.58,0,0,1-1.78,7.79,7.78,7.78,0,0,1-6,3.39,7.49,7.49,0,0,1-4.78-1.31v-.08a7.47,7.47,0,0,0,4.65,1,7.56,7.56,0,0,0,5.8-3.24,10.5,10.5,0,0,0,1.79-7.24,13.63,13.63,0,0,0-1.52-5.5,7.67,7.67,0,0,0-3.33-3.34Z" transform="translate(50.82 59)"/>
                    <path class="cls-89" d="M262.63,117.73l-1.84,2.19,1.19,15,1.93,1.56-6.72.53,1.9-1.86-.78-9.83-3.62,13.1-1.4.11-.15-.38,1.27-.11,3.8-14-.18-2.24-4.24,15.43-1,.08-4.94-11.46.61,7.72a8.39,8.39,0,0,0,.38,2.17,3.73,3.73,0,0,0,1.08,1.34l1.45,1.56-7.64.61-.3-.37,7.17-.57-.6-.76-6.51.52.29-.19a5.48,5.48,0,0,0,2-2,6.27,6.27,0,0,0,.29-2.93l-1-12.07L243,119.29l7.54-.6-2,2.08,5.1,11.89.43-1.31-4.6-10.54,1.8-1.57.33.23-1.73,1.43,4.29,9.9,2.94-10.71-1.94-1.77Zm-16.26,4.77.84,10.49a8,8,0,0,0,.72,3.28,2.85,2.85,0,0,0,2,1.16l-.15-.21a3.28,3.28,0,0,1-1.66-2.75l-.09-1.08-.68-8.63Zm17.16-3.7L262,120.12l1.17,14.72,2.77,2.69-7.76.61-.23-.38,7.07-.56-2.22-2.13-1.2-15.17,1.63-1.32Z" transform="translate(50.82 59)"/>
                    <path class="cls-90" d="M280.66,116.3l-.21.29a14.41,14.41,0,0,0-2,4.38c-.12-.21-.21-.37-.26-.48a4.75,4.75,0,0,0-1.91-2.12,7.27,7.27,0,0,0-3.34-.25c-.73.06-1.6.15-2.62.28l.53,6.72.94-.08-.47-5.94a20.66,20.66,0,0,1,2.11-.27,7,7,0,0,1,2.76.18,3.65,3.65,0,0,1,1.68,1.4l-.31-.19a6.63,6.63,0,0,0-1.79-1,7.82,7.82,0,0,0-2.09,0l-1.66.19-.31,0,.44,5.55,2.69-.21,1.68-2.73.5,6.29-2.08-2.29-4,.32.65,8.19.93-.08-.6-7.5,3.17-.25.41.35-3.18.25.57,7.13a15.3,15.3,0,0,0,4.2-.76,3.57,3.57,0,0,0,1.84-1.83l.66-1.39.15-.33a9.38,9.38,0,0,0,1.15,2.49,11.45,11.45,0,0,0,2,2.1l-16.37,1.31,2.09-1.87-.52-6.52a5.4,5.4,0,0,0-2.16.94l-.23-.29a5.21,5.21,0,0,1,2.36-1l-.06-.68a2,2,0,0,0-.35,0,4,4,0,0,0-2.63,1.31,2.93,2.93,0,0,1,2.88-2.58l-.49-6.24-2.36-1.57Zm3.93,18.42-.65,1.37L268,137.36l-.26-.37,16-1.27.42-1a11.28,11.28,0,0,1-3.29-3.92l.48.14a11.82,11.82,0,0,0,3,3.52Zm-7-12.25.51,6.45-.38-.16-.52-6.45Zm3.9-5.21-.09.14a17.24,17.24,0,0,0-2.06,4.21l-.27-.29a17.42,17.42,0,0,1,2.19-4.35Z" transform="translate(50.82 59)"/>
                    <path class="cls-91" d="M307.18,114.19c-.08.17-.13.29-.15.35l-.85,1.83a12.46,12.46,0,0,0-.77,2.41l0,.08-.16-.09-.05-.19a4.34,4.34,0,0,0-1.39-2.21,3.69,3.69,0,0,0-2.44-.45c-.44,0-.88.09-1.31.16h-.18l1.26,15.83,1.85,1.46-6.67.53,1.91-1.87-1.16-14.53-1,.06a2.42,2.42,0,0,0-2.67,2.91l-.38,0a3.6,3.6,0,0,1,.69-2.45,3.45,3.45,0,0,1,2.33-.87l.88,0H297l0-.72a11.14,11.14,0,0,0-1.43,0,3.92,3.92,0,0,0-2.65,1,3.61,3.61,0,0,0-.72,2.66l-.17,0a27.93,27.93,0,0,0-2.3-4.37l-.1-.14Zm-2.73,3.43a4.78,4.78,0,0,0-2.68-.52,5.9,5.9,0,0,0-.7.1l1.22,14.67,2.54,2.56-7.46.6-.36-.37,7-.56L301.87,132l-1.21-15.21.78-.08a6.38,6.38,0,0,1,2,.07A1.66,1.66,0,0,1,304.45,117.62Zm3.72-2.58a16.75,16.75,0,0,0-1.78,4.43l0,.15-.35-.25,0-.14a14.3,14.3,0,0,1,1.75-4.37l.07-.09Z" transform="translate(50.82 59)"/>
                    <path class="cls-92" d="M316.38,113.27a6.87,6.87,0,0,1,5.88,2.23,11.31,11.31,0,0,1,2.7,7,10.29,10.29,0,0,1-1.56,6.94,6.91,6.91,0,0,1-5.43,3,7.61,7.61,0,0,1-5.1-1.38c-2.27-1.59-3.55-4.1-3.82-7.53a10.61,10.61,0,0,1,1.63-7.19A7.35,7.35,0,0,1,316.38,113.27Zm.1,1.25q-4.94.39-4.25,9.07.63,8,5.5,7.63a3.76,3.76,0,0,0,3.28-2.4,13.24,13.24,0,0,0,.69-6.31q-.37-4.62-1.66-6.36A3.83,3.83,0,0,0,316.48,114.52Zm3.3,2-.26-.13a5.49,5.49,0,0,0-2.64-.57q-3.84.3-3.24,7.85c.3,3.68,1.28,6.06,3,7.13l-.12.16a5.71,5.71,0,0,1-2.3-2.56,13.67,13.67,0,0,1-1-4.52q-.65-8.1,3.61-8.44A3.26,3.26,0,0,1,319.78,116.47Zm1.31-2.83a7.2,7.2,0,0,1,3.58,3.19,12.48,12.48,0,0,1,1.66,5.54,11.57,11.57,0,0,1-1.79,7.78,7.77,7.77,0,0,1-6,3.39,7.51,7.51,0,0,1-4.78-1.31v-.07a7.47,7.47,0,0,0,4.65,1,7.56,7.56,0,0,0,5.79-3.23,10.53,10.53,0,0,0,1.79-7.24,13.63,13.63,0,0,0-1.51-5.51,7.83,7.83,0,0,0-3.33-3.34Z" transform="translate(50.82 59)"/>
                    <path class="cls-93" d="M116.3,165.47l6.56-.52q4.17-.33,6,.68a4.56,4.56,0,0,1,2.57,3.84,4.39,4.39,0,0,1-.65,2.78,4.74,4.74,0,0,1-2.27,1.86,17.34,17.34,0,0,1-4.88.9l-1.8.15.56,7.06,1.91,1.47-6.79.54,1.89-1.87L119,176.5h-.25a4.67,4.67,0,0,0-2,.61l-.15-.25a3.65,3.65,0,0,1,1.71-.7l.52,0,.12,0-.06-.72h-.45a3.91,3.91,0,0,0-2.44,1l-.17-.21a3.06,3.06,0,0,1,2.74-1.86l.23,0-.57-7.15Zm14.13.45a3.25,3.25,0,0,1,1.55,1.43,5.55,5.55,0,0,1,.74,2.46,4.84,4.84,0,0,1-2.09,4.75c-1.18.8-3.27,1.32-6.25,1.56-.5,0-.92.06-1.26.07l.48,6,2.51,2.53-7.32.58-.43-.36,6.88-.55-2-2-.52-6.59c.85,0,1.75-.06,2.71-.14q3.77-.3,5.48-1.77a4.7,4.7,0,0,0,1.5-4.22,5.26,5.26,0,0,0-.5-1.95A7.65,7.65,0,0,0,130.43,165.92Zm-9.25.68.58,7.31H122l.38,0,.19,0-.52-6.51,1.61-.13a7.33,7.33,0,0,1,2.66.12,2.84,2.84,0,0,1,1.42,1.26,2.89,2.89,0,0,0-1.44-.93,6.72,6.72,0,0,0-2.31-.12l-1.5.12.49,6.16a8.23,8.23,0,0,0,4.17-1.18,2.91,2.91,0,0,0,1.14-2.77A3.44,3.44,0,0,0,126.8,167a6.37,6.37,0,0,0-3.94-.61C122.4,166.44,121.84,166.51,121.18,166.6Z" transform="translate(50.82 59)"/>
                    <path class="cls-94" d="M133.52,164.1l6.86-.54-1.83,2.1.5,6.31a6.53,6.53,0,0,0,.79,0l-.49-6.25,1.46-1.7.32.24-1.39,1.61.49,6.09h.48c.76,0,1.48,0,2.19-.07a21.12,21.12,0,0,0,2.6-.35l-.54-6.72-1.91-1.49,6.75-.54-1.86,2.1,1.21,15.22,1.89,1.43-6.72.54,1.87-1.87-.5-6.19a10.86,10.86,0,0,1-2.5.49,23.69,23.69,0,0,1-2.55.07h-.21l.5,6.17,2.63,2.58L136,184l-.41-.36,7.12-.57-2.16-2L140,174.3h.42c1.3,0,2.23,0,2.8-.07a7.17,7.17,0,0,0,2.41-.57l0-.53a22.17,22.17,0,0,1-3.18.46c-.65,0-1.13.07-1.44.07l-1.48,0h-.34l.58,7.24,2,1.43-6.74.54,1.82-1.87-.51-6.38a2.34,2.34,0,0,0-1.88,1.12l-.32-.12a3.1,3.1,0,0,1,2.17-1.36l0-.54a3.11,3.11,0,0,0-2.49,1.45l-.23-.13a3.75,3.75,0,0,1,2.61-2.71l-.53-6.57Zm17-.52-1.38,1.56,1.19,14.9,2.64,2.58-7.58.6-.4-.36,7.12-.57-2.14-2L148.73,165l1.49-1.74Z" transform="translate(50.82 59)"/>
                    <path class="cls-95" d="M160.12,161.8A6.9,6.9,0,0,1,166,164a11.25,11.25,0,0,1,2.69,7,10.29,10.29,0,0,1-1.55,6.94,7,7,0,0,1-5.43,3,7.72,7.72,0,0,1-5.11-1.39q-3.41-2.37-3.81-7.53a10.53,10.53,0,0,1,1.63-7.18A7.34,7.34,0,0,1,160.12,161.8Zm.1,1.25q-4.93.4-4.25,9.07.65,8,5.51,7.63a3.75,3.75,0,0,0,3.27-2.4,13,13,0,0,0,.69-6.3q-.36-4.63-1.65-6.37A3.85,3.85,0,0,0,160.22,163.05Zm3.3,1.95-.26-.13a5.43,5.43,0,0,0-2.63-.57q-3.84.3-3.24,7.85.44,5.52,3,7.13l-.12.16a5.76,5.76,0,0,1-2.3-2.56,13.66,13.66,0,0,1-1-4.51q-.64-8.12,3.62-8.45A3.25,3.25,0,0,1,163.52,165Zm1.32-2.83a7.23,7.23,0,0,1,3.57,3.19,12.52,12.52,0,0,1,1.66,5.54,11.56,11.56,0,0,1-1.78,7.78,7.78,7.78,0,0,1-6,3.39,7.49,7.49,0,0,1-4.78-1.31v-.07a7.47,7.47,0,0,0,4.65,1,7.58,7.58,0,0,0,5.8-3.23,10.53,10.53,0,0,0,1.79-7.24,13.64,13.64,0,0,0-1.52-5.51,7.72,7.72,0,0,0-3.33-3.34Z" transform="translate(50.82 59)"/>
                    <path class="cls-96" d="M171.43,162.85a14.73,14.73,0,0,1,7.29-2.59,9.05,9.05,0,0,1,5.21.94,4,4,0,0,1,2.28,3.26,4.11,4.11,0,0,1-1.08,3.1,7.65,7.65,0,0,1-3.44,2.18,26,26,0,0,0,3.17,5.18,17.57,17.57,0,0,0,3.79,3.76l-.22.09a19.05,19.05,0,0,0-4.09,2.44l-.21.16-.11-.46a42.34,42.34,0,0,0-3.37-9.37l-.08-.14a10.9,10.9,0,0,1-2.64,1l-.11,0,.43,5.46,2.79,2.55-7.55.6-.26-.38,7-.55-2.3-2.08-.47-5.89.31-.07a9.53,9.53,0,0,0,2.73-.87,2,2,0,0,1-.12-.22l-.18-.41-.08-.17a23.83,23.83,0,0,1-3.38.84l-.15.05.51,6.51,1.81,1.6-6.46.52,1.76-1.89-.39-4.91-.11,0a14.22,14.22,0,0,0-1.5.54l-.12.05-.15-.28a6.79,6.79,0,0,1,1-.41l.76-.25.11,0-.06-.76a7.66,7.66,0,0,0-2.11.55l-.16.07a6.71,6.71,0,0,1,.22-1l.15-.68a.86.86,0,0,0,0-.13c.44,0,.78,0,1,0a5,5,0,0,0,.72-.1l-.57-7.18a8.85,8.85,0,0,0-1.44-.58Zm4.38-.73.63,7.87a3.83,3.83,0,0,0,.79-.13l-.57-7.19a7.89,7.89,0,0,1,1.91-.37q2.85-.23,4,1.56a7.75,7.75,0,0,0-2-1.05,5.18,5.18,0,0,0-2-.17,5.61,5.61,0,0,0-1.55.3l.54,6.89.16,0a9.61,9.61,0,0,0,4.14-2,3.61,3.61,0,0,0,1.16-3,3.33,3.33,0,0,0-1.47-2.58,4.75,4.75,0,0,0-3.25-.81A5.44,5.44,0,0,0,175.81,162.12Zm7.75,8.2a17.42,17.42,0,0,0,3.72,5.63,15.33,15.33,0,0,0,4,2.93,23.41,23.41,0,0,0-6.07,3.22l-.12.1-.31-.21a20.62,20.62,0,0,1,5.73-3.16q-4.42-2.47-7.33-8.44l-.11-.24q4.33-2.06,4.06-5.53a5.72,5.72,0,0,0-1.4-3,4.49,4.49,0,0,1,1.75,3.3Q187.7,168.25,183.56,170.32Z" transform="translate(50.82 59)"/>
                    <path class="cls-97" d="M206.27,158.32a6.13,6.13,0,0,0-2,1.75,6.23,6.23,0,0,0-.46,3.4l.08.95,1,12.87-2.47.19-8-12.83.64,7.93,0,.66.34,2.09a3.31,3.31,0,0,0,1.51,2.06l.12.09q-2.07-.51-2.36-4.15l-.09-1.37-.64-8-.91-1.47.67,8.41a16.54,16.54,0,0,0,.94,5.2,3.56,3.56,0,0,0,2.62,1.62l1.37,1.25-7.91.63-.27-.37,7.32-.59-.82-.74-6.81.54a4.1,4.1,0,0,0,2.23-1.91,8.43,8.43,0,0,0,.29-3.75l-.93-11.69-2.07-1.46,7.39-.59-2,1.55,8.16,13.23-.14-1.76-7-11.31,1.94-1.55.3.25-1.72,1.37,6.42,10.45-.54-6.81-.07-.78a9.06,9.06,0,0,0-.68-3.36,4.22,4.22,0,0,0-2.18-1.48Zm.59,1-.23.46a2.24,2.24,0,0,0-1.44,1.09,5.15,5.15,0,0,0-.22,2.42l1.2,15.06-3.14.25-.22-.38,2.93-.23-1.15-14.52a5.47,5.47,0,0,1,.31-2.91A2.91,2.91,0,0,1,206.86,159.36Z" transform="translate(50.82 59)"/>
                    <path class="cls-98" d="M215.74,157.38a6.9,6.9,0,0,1,5.89,2.23,11.25,11.25,0,0,1,2.69,7,10.29,10.29,0,0,1-1.55,6.94,7,7,0,0,1-5.43,3,7.72,7.72,0,0,1-5.11-1.39q-3.4-2.37-3.81-7.53a10.53,10.53,0,0,1,1.63-7.18A7.31,7.31,0,0,1,215.74,157.38Zm.1,1.25q-4.93.41-4.24,9.07.63,8,5.5,7.63a3.75,3.75,0,0,0,3.27-2.4,13,13,0,0,0,.69-6.31q-.36-4.62-1.65-6.36A3.85,3.85,0,0,0,215.84,158.63Zm3.3,2-.26-.13a5.43,5.43,0,0,0-2.63-.57q-3.84.3-3.24,7.85.44,5.52,3,7.13l-.12.16a5.76,5.76,0,0,1-2.3-2.56,13.66,13.66,0,0,1-1-4.51q-.65-8.12,3.62-8.45A3.24,3.24,0,0,1,219.14,160.58Zm1.32-2.83a7.19,7.19,0,0,1,3.57,3.19,12.48,12.48,0,0,1,1.66,5.54,11.56,11.56,0,0,1-1.78,7.78,7.78,7.78,0,0,1-6,3.39,7.49,7.49,0,0,1-4.78-1.31v-.07a7.47,7.47,0,0,0,4.65,1,7.56,7.56,0,0,0,5.8-3.23,10.53,10.53,0,0,0,1.79-7.24,13.64,13.64,0,0,0-1.52-5.51,7.72,7.72,0,0,0-3.33-3.34Z" transform="translate(50.82 59)"/>
                    <path class="cls-99" d="M242.51,155.44c-.08.17-.12.29-.15.35l-.85,1.83a12.58,12.58,0,0,0-.77,2.42l0,.07-.15-.09-.06-.19a4.39,4.39,0,0,0-1.39-2.21,3.69,3.69,0,0,0-2.44-.45c-.44,0-.87.09-1.31.16l-.17,0,1.25,15.83,1.85,1.45-6.67.53,1.91-1.87-1.16-14.53-1,.07a2.41,2.41,0,0,0-2.68,2.9l-.38,0a3.67,3.67,0,0,1,.69-2.45,3.48,3.48,0,0,1,2.34-.86l.87-.06h.16l0-.72a13.3,13.3,0,0,0-1.43,0,4,4,0,0,0-2.65,1,3.7,3.7,0,0,0-.72,2.67h-.17A27.11,27.11,0,0,0,225,157l-.1-.14Zm-2.73,3.44a4.68,4.68,0,0,0-2.68-.52,4.37,4.37,0,0,0-.7.1l1.22,14.67,2.54,2.56-7.46.59-.36-.37,7-.55-2.13-2.08L236,158.06l.78-.08a6.38,6.38,0,0,1,2,.07A1.68,1.68,0,0,1,239.78,158.88Zm3.72-2.59a16.9,16.9,0,0,0-1.78,4.43l0,.15-.35-.25,0-.14a14.41,14.41,0,0,1,1.75-4.37l.07-.09Z" transform="translate(50.82 59)"/>
                    <path class="cls-100" d="M247.42,154.93a5,5,0,0,1,.6,2c.19,2.42-1.12,4.22-3.92,5.38l-.14-.21.14-.13a3.86,3.86,0,0,0,1.26-3.14,3.24,3.24,0,0,0-.94-2.1,1.41,1.41,0,0,1,.27-.12A13.44,13.44,0,0,0,247.42,154.93Zm1.48.33a6.24,6.24,0,0,1,.38,1.6c.22,2.75-1.24,4.87-4.36,6.36l-.22-.38.24-.11a7.29,7.29,0,0,0,3.23-2.68,5.43,5.43,0,0,0,.72-3.06,9.52,9.52,0,0,0-.41-1.9Z" transform="translate(50.82 59)"/>
                    <path class="cls-101" d="M263.84,153.91a9.58,9.58,0,0,0-1,2.23c-.48,1.5-.8,2.43-1,2.78l-.07.09c0-.1-.08-.18-.1-.24a14,14,0,0,0-.82-1.77,3.4,3.4,0,0,0-.87-.92,4.41,4.41,0,0,0-3.11-.94,3.89,3.89,0,0,0-2.53,1.07,2.74,2.74,0,0,0-.87,2.27c.13,1.64,1.45,2.65,4,3.05l2.52.4q5.37.85,5.71,5.15a5.22,5.22,0,0,1-1.48,4.16,6.4,6.4,0,0,1-4.33,2,15.71,15.71,0,0,1-3.77-.22l-1.74-.29a8,8,0,0,0-1.73-.11,3.61,3.61,0,0,0-1.79.72l-.14-.2a19.11,19.11,0,0,0,2.13-5.4l.27.05a5.34,5.34,0,0,0,2.12,3.42,6.1,6.1,0,0,0,4.12,1,4.94,4.94,0,0,0,3-1.15,3,3,0,0,0,1-2.53,3.49,3.49,0,0,0-1.84-2.79,13.75,13.75,0,0,0-4.85-1.15,7.63,7.63,0,0,1-4.15-1.47,4.78,4.78,0,0,1-1.51-3.4,4.7,4.7,0,0,1,1.36-3.87,6.31,6.31,0,0,1,4.2-1.78,15.56,15.56,0,0,1,4,.29,6.53,6.53,0,0,0,1.6.17,3.17,3.17,0,0,0,1.52-.62Zm.74,9.31a4.87,4.87,0,0,1,2.31,4,6.11,6.11,0,0,1-1.78,4.9,7.58,7.58,0,0,1-5.1,2.35,11.28,11.28,0,0,1-2.88-.09l-2.38-.39a7.35,7.35,0,0,0-1.77-.06,3.43,3.43,0,0,0-1.5.46l-.21-.41a3.66,3.66,0,0,1,1.52-.48,9.08,9.08,0,0,1,2,.11l2.29.36a12.87,12.87,0,0,0,2.86.1,7.17,7.17,0,0,0,4.82-2.26,5.89,5.89,0,0,0,1.7-4.67,5.58,5.58,0,0,0-1.89-3.79Zm-12.26.45.52.24c.36.55,1.34,1,2.93,1.21l2.39.39a15.76,15.76,0,0,1,3.28.78,2.4,2.4,0,0,1,1.23,1.22l-.09.06a4.23,4.23,0,0,0-1.52-1,17.11,17.11,0,0,0-3-.63,22.78,22.78,0,0,1-4.33-.93A2.16,2.16,0,0,1,252.32,163.67Zm8.44-6a4.71,4.71,0,0,0-3.58-1.43,2.68,2.68,0,0,0-1.79.79,2.1,2.1,0,0,0-.6,1.69,3.07,3.07,0,0,0,1.65,2.1l.13.07-.07.12a2.71,2.71,0,0,1-2.12-2.34,2.35,2.35,0,0,1,.67-1.92,3.1,3.1,0,0,1,2.07-.89,4.53,4.53,0,0,1,2.24.36,3.22,3.22,0,0,1,1.5,1.39Zm-6.24,10.57a5,5,0,0,0,3.19,3.49v.12a4.48,4.48,0,0,1-3.59-3.51Zm10.28-13.81-.12.23q-.51,1-.87,2c-.57,1.48-.89,2.31-1,2.48l-.07.11-.36-.12.05-.13c.23-.67.57-1.54,1-2.61l.84-2,.06-.12Z" transform="translate(50.82 59)"/>
                    <path class="cls-102" d="M274.5,152.9l15.16-1.21a18.26,18.26,0,0,0-1.14,2,16.92,16.92,0,0,0-.81,2.28l-.1.31a6.16,6.16,0,0,0-2.11-2.42,6.34,6.34,0,0,0-3.22-.33l-.83.08-1.42.09-.54,0,.54,6.84.89-.07-.48-6,.43,0c.6-.08,1.24-.15,1.92-.2,2-.16,3.27.26,3.85,1.25a3.71,3.71,0,0,0-1.63-.8,10.1,10.1,0,0,0-2.61,0c-.42,0-.94.1-1.56.2l.45,5.64,2.2-.18,2.14-2.65.49,6.12-2.65-2.23-3.34.26.59,7.36,2,1.75-7.17.57,2.23-2.08-.5-6.28a2.72,2.72,0,0,0-1.91,1l-.3-.28a2.93,2.93,0,0,1,2.18-1.12l-.06-.68a3.1,3.1,0,0,0-.44,0,3.05,3.05,0,0,0-2.26,1.3l-.12-.23a3,3,0,0,1,2.72-2.3l-.49-6.16Zm10,9.77-3,.24.5,6.25,2.82,3-8.45.67-.4-.36,8-.64-2.32-2.45-.54-6.81,2.93-.24Zm2.26-4.62.5,6.31-.4-.19-.5-6.3Zm4.1-5.55a11.9,11.9,0,0,0-2.27,4.67l-.33-.18a10.1,10.1,0,0,1,2.32-4.72Z" transform="translate(50.82 59)"/>
                    <path class="cls-103" d="M290.67,151.61l6.92-.55-1.8,2.15,1.21,15.2,2,1.4-6.8.54,1.89-1.84-1.21-15.23Zm7.64.24-1.37,1.59,1.18,14.88,2.67,2.55-7.59.6-.4-.36,7.1-.57-2.18-2-1.2-15.16,1.52-1.77Z" transform="translate(50.82 59)"/>
                    <path class="cls-104" d="M316.36,150.09l-2.3,5.59a5.83,5.83,0,0,0-1.72-3.39,4.12,4.12,0,0,0-3.2-1,5,5,0,0,0-4.1,2.47,10.25,10.25,0,0,0-1.05,6.1q.67,8.45,7.59,7.9l2.36-.19-.61-7.57-3.68.29a7.94,7.94,0,0,1-1.72,0,2.33,2.33,0,0,1-1.05-.49l0-.36a2.86,2.86,0,0,0,1.1.43,8,8,0,0,0,1.71,0l3.64-.29-.06-.79-4.06.33-.66.05c-1.45.11-2.22-.35-2.3-1.4a1.65,1.65,0,0,1,.28-1.09,1.08,1.08,0,0,1,.84-.5.93.93,0,0,1,.69.22.91.91,0,0,1,.34.64c0,.42-.11.69-.41.78l-.35.1c-.11,0-.17.09-.16.16s.17.18.5.15l.49,0,9.64-.77-1.86,2.14.57,7.2,2,1.45-7.46.6-1.48.11a8.78,8.78,0,0,1-3.1-.21,7.38,7.38,0,0,1-2.58-1.41,9.29,9.29,0,0,1-3.51-7,9.58,9.58,0,0,1,2.2-7.5,8.14,8.14,0,0,1,5.57-2.75,13.58,13.58,0,0,1,3.46.26,12.65,12.65,0,0,0,2.64.33A4.18,4.18,0,0,0,316.36,150.09Zm-3.54,3.44-.28-.17a5.44,5.44,0,0,0-3.13-.94,4,4,0,0,0-2.78,1.41,5.13,5.13,0,0,0-1.09,2.39,15,15,0,0,0-.09,3.77,13.4,13.4,0,0,0,.95,4.47,6.56,6.56,0,0,0,2.37,2.64v.17a5.79,5.79,0,0,1-2.66-2.65,13.29,13.29,0,0,1-1.14-4.91,9.48,9.48,0,0,1,.87-5.49,4.18,4.18,0,0,1,3.5-2.21,4,4,0,0,1,3.46,1.43Zm5.95,4.7-1.38,1.62.54,6.85,2.65,2.6-5.8.41-5.25.48c-.33,0-.59,0-.76,0a3.82,3.82,0,0,1-1.92-.66l.3,0a6.93,6.93,0,0,0,2.75.22l.73-.07,9.1-.72-2.19-2.08-.57-7.15,1.54-1.83Zm-1.46-7.29-2.1,5.1-.37-.13,2.12-5.12Z" transform="translate(50.82 59)"/>
                    <path class="cls-105" d="M336.47,148l-.14.35-.86,1.84a13.25,13.25,0,0,0-.76,2.41l0,.07-.16-.09-.06-.18a4.31,4.31,0,0,0-1.39-2.22,3.77,3.77,0,0,0-2.43-.45c-.44,0-.88.09-1.31.16l-.18,0,1.26,15.83,1.85,1.45-6.67.53,1.9-1.87-1.15-14.53-1,.07a3.1,3.1,0,0,0-2.09.76,3.15,3.15,0,0,0-.59,2.14l-.38,0a3.67,3.67,0,0,1,.7-2.45,3.48,3.48,0,0,1,2.33-.86l.88,0,.16,0-.06-.71a13.26,13.26,0,0,0-1.42,0,4,4,0,0,0-2.66,1,3.7,3.7,0,0,0-.71,2.67h-.18a26.73,26.73,0,0,0-2.3-4.37l-.1-.14Zm-2.72,3.44a4.71,4.71,0,0,0-2.69-.52,6,6,0,0,0-.69.1l1.22,14.67,2.54,2.56-7.46.59-.37-.37,7-.55-2.14-2.08L330,150.59l.79-.07a6.18,6.18,0,0,1,2,.06A1.71,1.71,0,0,1,333.75,151.41Zm3.72-2.59a16.56,16.56,0,0,0-1.78,4.43l-.05.15-.34-.25,0-.13a14.15,14.15,0,0,1,1.76-4.37l.06-.1Z" transform="translate(50.82 59)"/>
                    <path class="cls-106" d="M337.85,147.87l6.86-.55-1.83,2.1.5,6.31a6.64,6.64,0,0,0,.8,0l-.5-6.25,1.47-1.7.31.24-1.39,1.61.49,6.09H345c.76,0,1.48,0,2.19-.07a21.12,21.12,0,0,0,2.6-.35l-.54-6.72-1.91-1.49,6.75-.54-1.86,2.1,1.21,15.22,1.89,1.44-6.72.53,1.87-1.87-.5-6.19a10.86,10.86,0,0,1-2.5.49,23.69,23.69,0,0,1-2.55.07h-.2l.49,6.17,2.63,2.59-7.55.6-.41-.37,7.12-.56-2.16-2-.53-6.72h.42c1.3,0,2.23,0,2.81-.08a7.17,7.17,0,0,0,2.4-.57l0-.53a22.17,22.17,0,0,1-3.18.46c-.65,0-1.13.07-1.43.07h-1.83l.58,7.25,2,1.42-6.74.54,1.82-1.86-.51-6.38a2.32,2.32,0,0,0-1.88,1.11l-.32-.12a3.1,3.1,0,0,1,2.17-1.36l0-.54a3.11,3.11,0,0,0-2.49,1.45l-.23-.13a3.75,3.75,0,0,1,2.61-2.71l-.52-6.57Zm17-.53-1.38,1.56,1.19,14.9,2.64,2.58-7.58.6-.39-.36,7.11-.57L354.27,164l-1.21-15.21,1.49-1.74Z" transform="translate(50.82 59)"/>
                    <path class="cls-107" d="M370,145.47a9.78,9.78,0,0,0-1,2.23,29.32,29.32,0,0,1-1,2.78l-.07.09a1.8,1.8,0,0,1-.1-.24,14.74,14.74,0,0,0-.82-1.77,3.23,3.23,0,0,0-.87-.91,4.43,4.43,0,0,0-3.11-1,4,4,0,0,0-2.54,1.08,2.74,2.74,0,0,0-.86,2.26q.19,2.46,4,3.05l2.52.41c3.58.56,5.48,2.28,5.71,5.14a5.23,5.23,0,0,1-1.48,4.17,6.44,6.44,0,0,1-4.33,2,15.21,15.21,0,0,1-3.77-.22l-1.74-.28a8.15,8.15,0,0,0-1.73-.12,3.61,3.61,0,0,0-1.79.72l-.14-.19a19.59,19.59,0,0,0,2.13-5.41l.27.06a5.35,5.35,0,0,0,2.12,3.41,6.1,6.1,0,0,0,4.12,1,4.94,4.94,0,0,0,3-1.16,3,3,0,0,0,1-2.53,3.49,3.49,0,0,0-1.84-2.79,13.81,13.81,0,0,0-4.85-1.14,7.71,7.71,0,0,1-4.15-1.48,5.29,5.29,0,0,1-.16-7.27,6.36,6.36,0,0,1,4.21-1.78,15.61,15.61,0,0,1,4,.3,6.93,6.93,0,0,0,1.6.16,3.07,3.07,0,0,0,1.52-.62Zm.74,9.32a4.89,4.89,0,0,1,2.31,4,6.09,6.09,0,0,1-1.78,4.89,7.64,7.64,0,0,1-5.11,2.36,11.66,11.66,0,0,1-2.87-.1l-2.38-.38a6.89,6.89,0,0,0-1.77-.07,3.56,3.56,0,0,0-1.5.46l-.21-.41A3.64,3.64,0,0,1,359,165a9,9,0,0,1,2,.1l2.29.36a11.75,11.75,0,0,0,2.86.1,7.19,7.19,0,0,0,4.82-2.26,5.88,5.88,0,0,0,1.7-4.67,5.53,5.53,0,0,0-1.89-3.78Zm-12.26.44.51.24c.37.55,1.35.95,2.94,1.21l2.39.4a16.66,16.66,0,0,1,3.28.77,2.44,2.44,0,0,1,1.23,1.22l-.1.07a4,4,0,0,0-1.51-1,19.25,19.25,0,0,0-3-.63,22.78,22.78,0,0,1-4.33-.93A2.12,2.12,0,0,1,358.48,155.23Zm8.44-6a4.72,4.72,0,0,0-3.58-1.44,2.68,2.68,0,0,0-1.79.79,2.11,2.11,0,0,0-.6,1.7,3.07,3.07,0,0,0,1.65,2.1l.12.06-.06.12a2.74,2.74,0,0,1-2.13-2.34,2.38,2.38,0,0,1,.67-1.92,3.14,3.14,0,0,1,2.08-.88,4.42,4.42,0,0,1,2.24.35,3.22,3.22,0,0,1,1.5,1.39Zm-6.24,10.56a5,5,0,0,0,3.18,3.49v.12a4.48,4.48,0,0,1-3.58-3.51ZM371,146l-.11.23a20.39,20.39,0,0,0-.87,2q-.86,2.24-1,2.49l-.07.11-.36-.12.05-.14c.23-.66.57-1.53,1-2.61l.84-2,.06-.13Z" transform="translate(50.82 59)"/>
                </g>
            </g>
        
        </g>
    </g>
</svg>
