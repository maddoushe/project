<?php
    if (isset($_FILES['avatar']) AND !empty($_FILES['avatar']['name'])) {
        $taillemax =  2097152;
        $extentionsvalidate = array('jpg', 'jpeg', 'gif', 'png');

        if ($_FILES['avatar']['size'] <= $taillemax) {
            $extentionupload = substr(strrchr($_FILES['avatar']['name'],'.'), 1);

            if (in_array($extentionupload, $extentionsvalidate)) {
                $chemin = 'avatars/'.$_SESSION['id'].'.'.$extentionupload;
                $deplacement = move_uploaded_file($_FILES['avatar']['tmp_name'], $chemin );

                if ($deplacement==true) {
                    $updateavatar = $bdd->prepare('UPDATE membre SET avatar = :avatar WHERE id = :id');
                    $updateavatar->execute(array(
                        'avatar' => $_SESSION['id'].'.'.$extentionupload,
                        'id'     => $_SESSION['id']
                        ));

                    header('Location: profil.php?id='.$_SESSION['id']);   

                }else {
                    $msgerror = "Erreur durent l'importation d de votre photo de profil";
                    echo  " echo Erreur durent l'importation d de votre photo de profil";
                }
                
            }else {
                $msgerror = ' votre photo de prfil doit etre au format jpg, jpeg, gif ou png';
                echo ' echo  votre photo de prfil doit etre au format jpg, jpeg, gif ou png';
            }

        }else{
            $msgerror = 'votre phpoto ne doit pas depasser la taille de 2 M.O.';   
            echo 'echo votre phpoto ne doit pas depasser la taille de 2 M.O.';    
        }
    }
?>