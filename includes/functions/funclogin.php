<?php 
    include_once'./includes/functions/data/connecteur.php';
    
    if (isset($_POST['connecter'])) {
        try {
            $utilis = htmlspecialchars($_POST['userlogin']);
            $passmot = sha1($_POST['passelogin']);
        } catch (PDOException $e) {
            return $e->getMessage();
        }
        
        if (!empty($utilis) AND !empty($passmot)) {
            try {
                $requtilisateur = $bdd->prepare('SELECT * FROM membre WHERE utilisateur = ? AND motpasse = ?');
                $requtilisateur->execute(array($utilis, $passmot));
                $utilisateurexist = $requtilisateur->rowCount();
            } catch (PDOException $e) {  
                return $e->getMessage();
            }

            if ($utilisateurexist == 1) {
                try {
                    $utilisateurconnect = $requtilisateur->fetch();
                    $_SESSION['id'] = $utilisateurconnect['id'];
                    $_SESSION['utilisateur'] = $utilisateurconnect['utilisateur'];
                    $_SESSION['motpasse'] = $utilisateurconnect['motpasse'];
                    header('Location: profil.php?id='.$_SESSION['id']);
                } catch (PDOException $e) {
                    return $e->getMessage();
                }

            }else {

            ?>
                <p style="color:orange" align="center"> Mauvais email ou utilisateur </p>;
            <?php
                
            }

        }else {
?>
           <p style="color:orange" align="center"> Tous les champs doivent être remplis </p>;
<?php
        }
    }
?>




