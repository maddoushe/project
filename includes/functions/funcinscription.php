<?php 
  include_once'./includes/functions/data/connecteur.php'; 
  $bdd = new PDO('mysql:host=127.0.0.1;dbname=projet','root','');

  
  if(isset($_POST['confirm'])) {

    $nom          = htmlspecialchars($_POST['nom']);
    $prenom       = htmlspecialchars($_POST['prenom']);
    $dateachat    = htmlspecialchars($_POST['dateachat']);
    $courriel     = htmlspecialchars($_POST['courriel']);
    $utilisateur  = htmlspecialchars($_POST['utilisateur']);
    $motpasse     = sha1($_POST['motpasse']);
    $motpasse2    = sha1($_POST['motpasse2']);

    if( !empty($_POST['nom']) AND !empty($_POST['prenom']) 
        AND !empty($_POST['courriel']) AND !empty($_POST['utilisateur']) 
        AND !empty($_POST['motpasse']) AND !empty($_POST['motpasse2'])  ){
      
      $nomLenght  = strlen($nom);
      $prenomLenght  = strlen($prenom);
      $courrielLenght  = strlen($courriel);
      $utilisateurLenght  = strlen($utilisateur);
      $motpasseLenght  = strlen($motpasse);


      if ($nomLenght <= 255 AND $prenomLenght <= 255 AND $courrielLenght <= 255 AND $utilisateurLenght <= 255 AND $motpasseLenght <= 255 ){ 

        $reqcouriel = $bdd->prepare('SELECT * FROM member WHERE courriel = ?');
        $reqcouriel->execute(array($courriel));
        $courrielexist = $reqcouriel->rowCount();

        if ($courrielexist == 0) {

          if ($motpasse == $motpasse2) {
            
            try {
              $insertmembre = $bdd->prepare("INSERT INTO member(nom, prenom, dateachat,  courriel, utilisateur, motpasse ) 
              VALUE(?, ?, now(), ?, ?, ?)");
              $test = $insertmembre->execute(array($nom, $prenom,  $courriel, $utilisateur, $motpasse));
            } catch (PDOException $e){
              echo  $e->getMessage();
            }
            $erreur = "votre compte a ete bien creer. <a class='btn' href=\"profil.php\">Voir mon profil</a>";

          }else {
            $erreur = 'vos mots de passe ne sont pas identiques';
          }

        }else {
          $erreur = 'votre courriel a ete deja utilise';
        }

      }else {
        $erreur = 'votre pseudo ne doit pas depasser 255 characteres';
      }

    }else{
      $erreur = "tous les champs doivent etre remplis" ;
    }
    
  }

?>
