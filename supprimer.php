<?php
    // Appelle la base des données
    include_once'./includes/functions/data/connecteur.php';
    
    if (isset($_GET["id"]) AND !empty($_GET["id"])) {
        $supprimer_article_id = htmlspecialchars($_GET["id"]);
        $supprimer_article = $bdd->prepare("DELETE FROM produit WHERE id = ?");
        $supprimer_article->execute(array($supprimer_article_id));
        header("Location: panier.php");
    }
?>