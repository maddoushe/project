-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 02 juil. 2020 à 02:27
-- Version du serveur :  10.3.20-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `dateachat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `courriel` varchar(255) NOT NULL,
  `utilisateur` varchar(255) NOT NULL,
  `motpasse` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `member`
--

INSERT INTO `member` (`id`, `nom`, `prenom`, `dateachat`, `courriel`, `utilisateur`, `motpasse`) VALUES
(1, 'qqq', 'qqq', '2020-06-29 04:00:00', 'qqq@qqq.com', 'qqq', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441'),
(2, 'www', 'www', '2020-06-23 04:00:00', 'www@www.com', 'www', 'c50267b906a652f2142cfab006e215c9f6fdc8a0'),
(3, 'aaa', 'aaa', '2020-06-30 21:15:48', 'aaa@aaa.com', 'aaa', '7e240de74fb1ed08fa08d38063f6a6a91462a815'),
(4, 'yyy', 'yyy', '2020-06-30 21:17:01', 'yyy@yyy.com', 'yyy', '186154712b2d5f6791d85b9a0987b98fa231779c');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prix` float NOT NULL,
  `image` int(11) NOT NULL,
  `descrip` text NOT NULL,
  `datelivraison` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id`, `nom`, `prix`, `image`, `descrip`, `datelivraison`) VALUES
(1, 'La version de gros gros luxe', 10287.2, 1, 'Bonne pour 2 utilisations Elle n\'attire aucune molécule nocive. Pour plus de sécurité elle est trempée dans un plastique qui la rend monobloc et impénétrable qui elle est recouverte d’une couche d’acier pour plus d’impénétrabilité. De plus une couverture de fonte permet de ne pas le perdre car au poids qu\'il a vous ne pouvez l\'oublier! (Plus c\'est lourd plus ça vaut cher non?). Elle a deux choix de couleurs vive (tout le monde sait que la couleur ça coute cher) Rouge ou brun vif. ', '2020-07-01 21:20:11'),
(2, 'La version de pas base', 3050.99, 2, 'Bonne pour deux utilisations.\r\nElle attire 25 % des molécule atomique 10 pied autour de l\'utilisateur.\r\n Pour plus de sécurité elle est trempée dans un plastique qui la rend\r\nmonobloc et impénétrable. Elle est de couleur pastel', '2020-07-01 21:20:11'),
(3, 'La version de base à ', 1250.54, 3, 'Bonne pour deux utilisations.\r\nElle attire 50 % des molécule atomique 15 pied autour de l\'utilisateur.\r\nPour plus de sécurité elle est impossible à modifier. Si la boite est ouverte\r\nelle explose.Elle est blanche (tout le monde sait que la couleur ça coute cher)', '2020-07-01 21:20:11');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
