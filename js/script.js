/****************************************/
/*     Boutique images gallerie         */
/****************************************/
//armes
$(document).ready(function () {
  $("#arme img").on({ 
    mouseover: function () {
      $(this).css({
        "cursor":"pointer",
        "border-Color": "red"
      });
    },
    
    mouseout: function () {
      $(this).css({
        "cursor":"default",
        "border-Color": "darkblue"
      });
    },
    click: function () {
      var imgUrl = $(this).attr("src");
      $("#mainImage").attr("src", imgUrl);
    }
  });
});
//armures
$(document).ready(function () {
  $("#armure img").on({ 
    mouseover: function () {
      $(this).css({
        "cursor":"pointer",
        "border-Color": "red"
      });
    },
    
    mouseout: function () {
      $(this).css({
        "cursor":"default",
        "border-Color": "darkblue"
      });
    },
    click: function () {
      var imgUrl = $(this).attr("src");
      $("#mainImage1").attr("src", imgUrl);
    }
  });
});

/****************************************/
/*             Anim menu                */
/****************************************/
// $(document).ready(function(){
//   var boxHeight = $(".li a").height();
//   $(".li a").mouseenter(function(){$(this).animate({height: "60"});}).mouseleave(function(){$(this).animate({height: boxHeight});});
// });



/****************************************/
/*    Animation page armes et armures   */
/****************************************/

$(function (e) {
  $(".li a" ).hover(function () {
    $(this).width(1.25*$(this).width());
    $(this).height(1.25*$(this).height()); 
  },function () {
    $(this).width(0.8*$(this).width());
    $(this).height(0.8*$(this).height());
  });
});

/****************************************/
/*              Logo                    */
/****************************************/
$(function() {doBounce($("#logo"), 5, '20px', 250);});
function doBounce(element, times, distance, speed) {
  for(var i = 0; i < times; i++) {
    element.animate({marginTop: '-='+distance}, speed).animate({marginTop: '+='+distance}, speed);
  }        
}

/****************************************/
/*              Modal                   */
/****************************************/
function blink_text() {
  $element = $('#avertissement');
  $element.fadeOut(3000);
  $element.fadeIn(1000);
}
setInterval(blink_text, 1000);

/****************************************/
/*         Animation pub index          */
/****************************************/
let path1 = anime.path('#trajethaut');
let path2 = anime.path('#trajetbas');
var path3 = anime.path('#trajetbras');

anime({
    targets: '#asteroide',
    translateX: path1('x'),
    translateY: path1('y'),
    easing: 'linear',
    duration: 5000,
});

anime({
  targets: '#epee',
  translateX: path2('x'),
  translateY: path2('y'),
  // rotate: path2('angle'),
  easing: 'linear',
  duration: 3100,
    
});
 
setTimeout( function(){$('#epee').hide();} , 2600);
setTimeout( function(){$('#asteroide').hide();} , 2600);

setTimeout( function(){$('#boom').hide();} , 0);
setTimeout( function(){$("#boom").show();}, 2700);

/****************************************/
/*         Menu Mobile avec JS          */
/****************************************/
/*lorseque on clique sur le bouton 
changement entre chacher/montrer le menu */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// fermiture du menu decoulant lorsque on clique ailleur
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}










